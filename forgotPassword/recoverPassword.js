(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                if (form.checkValidity() === false) {
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    form.classList.add('was-validated');
                    let url_string = window.location.href;
                    let url = new URL(url_string);
                    let key = url.searchParams.get("key");
                    let email = url.searchParams.get("email");

                    $.ajax({
                        url: "../servicos/recoverPassword.php",
                        type: "post",
                        dataType: "json",
                        data: $("#frm_cadastro").serialize() + `&key=${key}&email=${email}`,
                        success: function () {
                            swal("Pronto!", "Senha recadastrada com sucesso", "success").then(res => {
                                window.location = '../index.php';
                            });
                        },
                        error: function (error) {
                            swal("Erro!", error.responseText, "error");
                        }
                    })
                }
            }, false);
        });
    }, false);
})();

$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});