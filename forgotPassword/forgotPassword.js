(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                if (form.checkValidity() === false) {
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    form.classList.add('was-validated');
                    $.ajax({
                        url: "../servicos/forgotPassword.php",
                        type: "post",
                        dataType: "json",
                        data: $("#frm_cadastro").serialize(),
                        success: function () {
                            swal("Pronto!", "Um e-mail com instruções para a recuperação de senha foi enviado", "success");
                        },
                        error: function (error) {
                            swal("Erro!", error.responseText, "error");
                        }
                    })
                }
            }, false);
        });
    }, false);
})();

$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});