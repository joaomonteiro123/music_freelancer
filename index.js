(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('form-signin');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                if (form.checkValidity() === false) {
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    form.classList.add('was-validated');
                    $.ajax({
                        url: "servicos/login.php",
                        type: "post",
                        data: $("#frm_login").serialize(),
                        success: function (resultado) {
                            if (resultado == 'logado') {
                                window.location.replace('home/home.php');
                            } else {
                                swal("Erro", resultado, "error");
                            }
                        }
                    })
                }
            }, false);
        });

        setTimeout(_ => {
            $(".logo-inicio").addClass('logo-inicio-translacao');
            $(".form-signin").addClass('form-signin-translacao');
        }, 200)

    }, false);
})();