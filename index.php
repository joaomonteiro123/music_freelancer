<?php
error_reporting(E_WARNING);
session_start();
?>

<HTML>
<HEAD>
<meta charset="utf-8">
    <TITLE>Musitray - Sua música a um clique</TITLE>
    <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/estilo.css">
    <script src="js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/combobox.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
</HEAD>

<body class="text-center fundo-main">      
    <img class="logo-inicio" src="images/logo_clara_slogan.png">
    <form class="form-signin" id="frm_login">
        <input name="usuario" type="text" class="form-control input mb-1" placeholder="E-mail" required="" autofocus="">
        <input name="senha" type="password" class="form-control input" placeholder="Senha" required="">
        <button class="btn btn-lg btn-quadrado btn-block mt-4 mb-2" type="submit">Entrar</button>
        <small><a style="color: white;" href="signup/signup.php">Cadastre-se</a></small>
        <small style="color: white;"> | </small>
        <small><a style="color: white;" href="forgotPassword/forgotPassword.php">Esqueci minha senha</a></small>
    </form>
    <script src="js/popper.min.js" crossorigin="anonymous"></script>
    <script src="index.js" crossorigin="anonymous"></script>
    <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
</BODY>
</HTML>