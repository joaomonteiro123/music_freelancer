<?php 
include "../../sessao.php";
$nome = UTF8_DECODE($_POST['nome']);
if ($nome == "") $nome = UTF8_DECODE("Projeto sem título");

$query = "INSERT INTO `projeto` (`id`, `id_usuario`, `nome`, `data_criacao`)
            VALUES (NULL, (?), (?), CURDATE())";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query->bind_param("is", $idLogado, $nome)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}else{
    $projetoID = $conexao->insert_id;
}

$query = "INSERT INTO `projeto_instrumentista` (`id`, `id_projeto`)
            VALUES (NULL, (?))";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query->bind_param("i", $projetoID)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}else{
    $projetoInstrumentistaID = $conexao->insert_id;
}

$IDs = array();

$_SESSION['idProjeto'] = $projetoID;
$_SESSION['idProjetoInstrumentista'] = $projetoInstrumentistaID;

$conexao->close();
echo 1
?>