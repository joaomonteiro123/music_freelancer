<?php
include "../sessao.php";
?>

<HTML>
<HEAD>
<meta charset="utf-8">
    <TITLE>Musitray - Sua música a um clique</TITLE>
    <link rel="stylesheet" href="../bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/estilo.css">
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/combobox.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="../js/sweetalert.min.js"></script>
</HEAD>

<BODY>      
<?php 
    include "../cabecalho/cabecalho.php"
?>
<div class="container">
    <div class="row">
        <div class="col-12 mt-4 pt-4 pb-4 mb-4"><h1>Envie sua demanda</h1></div>
    </div>
    <div class="row">
        <div class="col-9">
            <div class="row">
                <div class="col-12">
                    <label><h3>Primeiro, vamos dar um nome ao seu projeto<h3></label>
                    <input maxlength="100" type="text" class="form-control mt-4 mb-1 input-quadrado" id="nome" placeholder="Nome" name="nome" required>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="button" class="btn btn-quadrado btn_criar">Criar projeto!</button>
                </div>
            </div>    
        </div>
    </div>
</div>
<?php 
    include "../rodape/rodape.php"
?>

    <script src="../js/popper.min.js" crossorigin="anonymous"></script>
    <script src="novoProjeto.js" crossorigin="anonymous"></script>
    <script src="../bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
</BODY>
</HTML>