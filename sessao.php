<?php
session_start();
include "servicos/conectasql.php";

$usuario = $_SESSION['usuario'];
$query = "SELECT * FROM usuario u WHERE u.email = (?)";

if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query->bind_param("s", $usuario)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}
$resultado = $query->get_result();
$user = $resultado ->fetch_assoc();

if ($resultado -> num_rows == 0){
    header('Location: ../index.php'); 
}else{
    $usuarioLogado = UTF8_ENCODE($user['primeiro_nome']);
    $idLogado = UTF8_ENCODE($user['id']);
}

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
} else {
    header('Location: ../index.php'); 
}
?>