<?php
include "conectasql.php";

//Checa o tamanho mínimo da senha
if (strlen($_POST['senha']) < 4){
    http_response_code(400);
    die("A senha precisa de, no mínimo, 4 caracteres!");
}

//Checa se as senhas são iguais
if ($_POST['senha'] != $_POST['senha2']){
    http_response_code(400);
    die("As senhas informadas são diferentes!");
}

// Checa se email existe no banco
$dateFormat = mktime(date("H"), date("i"), date("s"), date("m") ,date("d"), date("Y"));
$todaysDate = date("Y-m-d H:i:s",$dateFormat);
$query = "SELECT * FROM `forgot_password` WHERE `recover_key`=(?) AND `email`=(?) AND expDate > (?)";
if (!$query = $conexao->prepare($query)){
    var_dump('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query->bind_param("sss", $_POST["key"], $_POST["email"], $todaysDate)){
    var_dump('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    var_dump('execute() failed: ' . htmlspecialchars($conexao->error));
}
$resultado = $query->get_result();
if ($resultado -> num_rows == 0){
    http_response_code(400);
    die("O código enviado é inválido");
}


$senha = utf8_decode(mysqli_real_escape_string($conexao, $_POST["senha"])); 
$senha = md5($senha); 
$query = "UPDATE `usuario` SET senha = (?) WHERE `email`=(?)";
if (!$query = $conexao->prepare($query)){
    var_dump('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query->bind_param("ss", $senha, $_POST["email"])){
    var_dump('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    var_dump('execute() failed: ' . htmlspecialchars($conexao->error));
}

$query = "DELETE FROM `forgot_password` WHERE `recover_key`=(?) AND `email`=(?)";
if (!$query = $conexao->prepare($query)){
    var_dump('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query->bind_param("ss", $_POST["key"], $_POST["email"])){
    var_dump('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    var_dump('execute() failed: ' . htmlspecialchars($conexao->error));
}

echo json_encode(array());