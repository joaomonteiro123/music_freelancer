<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '../vendor/autoload.php';

include "conectasql.php";

if(isset($_POST["email"]) && (!empty($_POST["email"]))){
    $email = $_POST["email"];
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
}
if (!$email) {
    http_response_code(400);
    die("Um e-mail inválido foi inserido");
}

// Checa se email existe no banco
$query = "SELECT * FROM usuario u WHERE u.email = (?)";
$query = $conexao->prepare($query);
$query->bind_param("s", $email);
$query->execute();
$resultado = $query->get_result();
if ($resultado -> num_rows == 0){
    http_response_code(400);
    die("O e-mail informado não está cadastrado");
}

$expFormat = mktime(date("H"), date("i"), date("s"), date("m") ,date("d")+1, date("Y"));
$expDate = date("Y-m-d H:i:s",$expFormat);

$key = md5(2418*2+$email);
$addKey = substr(md5(uniqid(rand(),1)),3,10);
$key = $key . $addKey;

$query = "INSERT INTO forgot_password (email, recover_key, expDate) VALUES (?, ?, ?)";
$query = $conexao->prepare($query);
if ( false===$query ) {
    http_response_code(400);
    die('prepare() failed: ' . htmlspecialchars($mysqli->error));
} 
if ( false===$query->bind_param("sss", $email, $key, $expDate) ) {
    http_response_code(400);
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if ( false===$query->execute() ) {
    http_response_code(400);
    die('execute() failed: ' . htmlspecialchars($query->error));
}

$output='<p>Caro usuário Musitray,</p>';
$output.='<p>Para redefinir sua senha, basta clicar no link abaixo e escolher uma nova:</p>';
$output.='<p></p>';
$output.='<p><a href="https://musitray.com/forgotPassword/recoverPassword.php?key='.$key.'&email='.$email.'" target="_blank">
    https://musitray.com/forgotPassword/recoverPassword.php?key='.$key.'&email='.$email.'</a></p>';		
$output.='<p></p>';
$output.='<p>Caso você não tenha pedido a redefinição de senha, sugerimos que faça login no site e altere-a, pois pode ser que alguém esteja tentando entrar na sua conta.</p>';   	
$output.='<p>Obrigado por estar conosco!</p>';
$output.='<p>Equipe Musitray</p>';
$subject = "Recuperação de senha - Musitray";


$sender = 'noreply@musitray.com';
$senderName = 'Musitray';

$recipient = $email;

$usernameSmtp = 'AKIA3KEXBKJUCJC5V2ZM';
$passwordSmtp = 'BJX3VuKsyJ5In9gGELYixpnKWjXImgII/wO8wdb6IvX8';

$host = 'email-smtp.us-east-1.amazonaws.com';
$port = 587;

$bodyText =  $output;
$bodyHtml = $output;

$mail = new PHPMailer(true);

try {
    $mail->CharSet = 'UTF-8';
    $mail->Encoding = 'base64';
    $mail->isSMTP();
    $mail->setFrom($sender, $senderName);
    $mail->SMTPDebug  = 2;
    $mail->Username   = $usernameSmtp;
    $mail->Password   = $passwordSmtp;
    $mail->Host       = $host;
    $mail->Port       = $port;
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = 'tls';
    $mail->addCustomHeader('X-SES-CONFIGURATION-SET', $configurationSet);

    // Specify the message recipients.
    $mail->addAddress($recipient);
    // You can also add CC, BCC, and additional To recipients here.

    // Specify the content of the message.
    $mail->isHTML(true);
    $mail->Subject    = $subject;
    $mail->Body       = $bodyHtml;
    $mail->AltBody    = $bodyText;
    if(!$mail->Send()){
        http_response_code(400);
        die("Ocorreu um erro ao enviar o e-mail");
    }
} catch (phpmailerException $e) {
    http_response_code(400);
    echo "An error occurred. {$e->errorMessage()}", PHP_EOL; //Catch errors from PHPMailer.
} catch (Exception $e) {
    http_response_code(400);
    echo "Email not sent. {$mail->ErrorInfo}", PHP_EOL; //Catch errors from Amazon SES.
}

echo json_encode(array());