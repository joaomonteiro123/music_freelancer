<?php
    include "../servicos/conectasql.php";

    $query = "SELECT * FROM `novo_usuario` WHERE `key`=(?) AND `email`=(?)";

    if (!$query = $conexao->prepare($query)){
        var_dump('prepare() failed: ' . htmlspecialchars($conexao->error));
    }
    if (!$query->bind_param("ss", $_GET["key"], $_GET["email"])){
        var_dump('bind_param() failed: ' . htmlspecialchars($query->error));
    }
    if(!$query->execute()){
        var_dump('execute() failed: ' . htmlspecialchars($conexao->error));
    }
    $resultado = $query->get_result();

    if ($resultado -> num_rows > 0){
        $user = $resultado ->fetch_assoc();
        var_dump($user);

        $query = "UPDATE `usuario` set ativo = 1 WHERE id = (?)";

        if (!$query = $conexao->prepare($query)){
            var_dump('prepare() failed: ' . htmlspecialchars($conexao->error));
        }
        if (!$query->bind_param("i", $user['id_usuario'])){
            var_dump('bind_param() failed: ' . htmlspecialchars($query->error));
        }
        if(!$query->execute()){
            var_dump('execute() failed: ' . htmlspecialchars($conexao->error));
        }
    }

    header('Location: ../index.php');
?>