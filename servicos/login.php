<?php 
session_start();
include "conectasql.php";

$usuario = $_POST['usuario'];
$senha = md5($_POST['senha']);

$query = "SELECT * FROM usuario u WHERE u.email = (?) AND u.senha = (?)";
$query = $conexao->prepare($query);
$query->bind_param("ss", $usuario, $senha);
$query->execute();
$resultado = $query->get_result();
$user = $resultado ->fetch_assoc();

if ($resultado -> num_rows == 0){
    exit("E-mail ou senha incorretos");
}else if (!$user['ativo']){
    exit("Usuário inativo devido à não confirmação do e-mail");
}
else {
    $_SESSION['loggedin'] = true;
    $_SESSION['usuario'] = UTF8_ENCODE($user['email']);
    $_SESSION['idUsuario'] = $user['id'];
    exit('logado');
}

?>