(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                if (form.checkValidity() === false) {
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    form.classList.add('was-validated');
                    $.ajax({
                        url: "../servicos/cadastro.php",
                        type: "post",
                        data: $("#frm_cadastro").serialize(),
                        success: function (resultado) {
                            if (resultado == 'OK') {
                                window.location.replace("../");
                            } else {
                                swal("Erro", resultado, "error");
                            }
                        }
                    })
                }
            }, false);
        });
    }, false);
})();

$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});