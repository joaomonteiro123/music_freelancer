<HTML>
<HEAD>
<meta charset="utf-8">
    <TITLE>Musitray - Sua música a um clique</TITLE>
    <link rel="stylesheet" href="../bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/estilo.css">
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/combobox.js"></script>
    <script src="signup.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="../js/sweetalert.min.js"></script>
</HEAD>

<BODY>
<div class="container">
    <div class="py-5 text-center">
        <a class="navbar-brand" href="../index.php">
            <img class="d-block mx-auto mb-4" src="../images/logo.png" style="width: 35%;">
        </a>
        <label class="texto-principal grande">Cadastro de novo usuário</label>
    </div>

    <div class="row">
        <div class="col-8 offset-2">
            <form class="needs-validation" novalidate="" id="frm_cadastro">
                <div class="row">
                    <div class="col-6 mb-3">
                        <label for="primeiroNome" class="texto-principal small">Primeiro Nome</label>
                        <input type="text" class="form-control input" id="primeiroNome" placeholder="Primeiro Nome" name="primeiroNome" required="">
                        <div class="invalid-feedback">
                            Primeiro nome é necessário.
                        </div>
                    </div>
                    <div class="col-6 mb-3">
                        <label for="ultimoNome" class="texto-principal small">Último Nome</label>
                        <input type="text" class="form-control input" id="ultimoNome" placeholder="Último Nome" name="ultimoNome" required="">
                        <div class="invalid-feedback">
                            Último nome é necessário.
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="email" class="texto-principal small">Email</label>
                    <input type="email" class="form-control input" id="email" placeholder="E-mail" required="" name="email">
                    <div class="invalid-feedback">
                        É necessário informar um e-mail válido.
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 mb-3">
                        <label for="address" class="texto-principal small">Senha</label>
                        <input type="password" class="form-control input" id="senha" placeholder="Senha" required="" name="senha">
                        <div class="invalid-feedback">
                            É necessaário informar uma senha.
                        </div>
                    </div>
                    <div class="col-6 mb-3">
                        <label for="address2" class="texto-principal small">Confirmação de senha</label>
                        <input type="password" class="form-control input" id="senha_confirmacao" placeholder="Senha" required="" name="senha2">
                        <div class="invalid-feedback">
                            É necessaário confirmar a sua senha.
                        </div>
                    </div>
                </div>

                <hr class="mb-4">
                <button class="btn btn-quadrado w-100" type="submit">Cadastrar!</button>
            </form>
        </div>
    </div>
</div>

<div class="loading-modal">
    <svg class="loading-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgba(255, 255, 255, 0); display: block;" width="201px" height="201px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
    <g transform="rotate(180 50 50)"><rect x="2.761904761904762" y="31.25" width="4" height="10.3091" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.7142857142857142s"></animate>
        </rect><rect x="7.523809523809524" y="31.25" width="4" height="37.2134" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.11904761904761904s"></animate>
        </rect><rect x="12.285714285714286" y="31.25" width="4" height="6.63531" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.6547619047619048s"></animate>
        </rect><rect x="17.047619047619047" y="31.25" width="4" height="22.885" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.3571428571428571s"></animate>
        </rect><rect x="21.80952380952381" y="31.25" width="4" height="5.25778" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.5357142857142857s"></animate>
        </rect><rect x="26.571428571428573" y="31.25" width="4" height="24.0574" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.8928571428571428s"></animate>
        </rect><rect x="31.333333333333336" y="31.25" width="4" height="13.651" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.4166666666666666s"></animate>
        </rect><rect x="36.095238095238095" y="31.25" width="4" height="37.473" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.17857142857142855s"></animate>
        </rect><rect x="40.857142857142854" y="31.25" width="4" height="7.66924" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.47619047619047616s"></animate>
        </rect><rect x="45.61904761904762" y="31.25" width="4" height="35.8799" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.05952380952380952s"></animate>
        </rect><rect x="50.38095238095238" y="31.25" width="4" height="29.4146" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-1.1309523809523807s"></animate>
        </rect><rect x="55.142857142857146" y="31.25" width="4" height="26.6141" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-1.0714285714285714s"></animate>
        </rect><rect x="59.904761904761905" y="31.25" width="4" height="25.2844" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-1.0119047619047619s"></animate>
        </rect><rect x="64.66666666666667" y="31.25" width="4" height="24.9828" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.9523809523809523s"></animate>
        </rect><rect x="69.42857142857143" y="31.25" width="4" height="15.9884" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.7738095238095237s"></animate>
        </rect><rect x="74.19047619047619" y="31.25" width="4" height="33.075" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="0s"></animate>
        </rect><rect x="78.95238095238095" y="31.25" width="4" height="5.1567" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.5952380952380952s"></animate>
        </rect><rect x="83.71428571428571" y="31.25" width="4" height="31.3164" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.2976190476190476s"></animate>
        </rect><rect x="88.47619047619048" y="31.25" width="4" height="21.1825" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.8333333333333331s"></animate>
        </rect><rect x="93.23809523809524" y="31.25" width="4" height="35.9767" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.23809523809523808s"></animate>
        </rect>
    </g>
    </svg>
</div>

<?php 
    include "../rodape/rodape.php"
?>

<script src="../js/popper.min.js" crossorigin="anonymous"></script>
<script src="../bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
</BODY>


