Vue.component("profissionais", {
    props: ["profissional"],
    template: '#profissionais-template',
    methods: {
        abrePerfil: function (id) {
            window.open("../perfilProfissional/perfilProfissional.php?id=" + id);
        },
    },
})

Vue.component("instrumentos-filtro", {
    props: ["instrumento"],
    template: '#instrumentos-filtro-template',
    methods: {
        removeFiltro: function (id) {
            let i = encontreProfissionais.filtroInstrumentos.map(item => item.id).indexOf(id);
            encontreProfissionais.filtroInstrumentos.splice(i, 1);
        },
    },
})


encontreProfissionais = new Vue({
    el: "#app",
    data: {
        profissionais: [],
        filtroInstrumentos: [],
        filterizd: [],
    },
    created: function () {
        $("body").addClass("loading");
        this.$http.get("servicos/carregaProfissionais.php").then(function (resposta) {
            this.profissionais = resposta.data;
            $("body").removeClass("loading");
        });
    },
    updated: function () {
        //Autocomplete
        $("#inputInstrumento").autocomplete({
            source: "servicos/buscaInstrumentos.php",
            minLength: 0,
            select: function (event, ui) {
                if (ui.item == null) {
                    swal("Atenção", "Selecione um instrumento válido", "info");
                    $("#inputInstrumento").val("");
                    $("#inputInstrumento").focus();
                } else {
                    existe = false;
                    for (var i = 0; i < encontreProfissionais.filtroInstrumentos.length; i++) {
                        if (encontreProfissionais.filtroInstrumentos[i].id == ui.item['id']) {
                            existe = true;
                            break;
                        }
                    }
                    if (!existe) {
                        encontreProfissionais.filtroInstrumentos.push(ui.item);
                    } else {
                        swal("Atenção", "O instrumento selecionado já existe!", "info");
                    }
                    $("#inputInstrumento").val("");
                    $("#inputInstrumento").focus();
                }
            },
            change: function (event, ui) {
                $("#inputInstrumento").val("");
                $("#inputInstrumento").focus();
            }
        });
        $("#inputInstrumento").click(function () {
            $("#inputInstrumento").autocomplete("search", "");
        });

        $('#filtr-container').imagesLoaded(function () {
            filter = [];
            for (var i = 0; i < encontreProfissionais.filtroInstrumentos.length; i++) {
                filter.push(encontreProfissionais.filtroInstrumentos[i].id);
            }
            if (filter == "") {
                filter = "all"
            }
            var filterizd = $('.filtr-container').filterizr({
                filter: filter,
                layout: 'packed',
            });
        });
    },
    methods: {
    },
})