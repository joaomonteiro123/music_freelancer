<?php
include "../sessao.php";
?>

<HTML>
<HEAD>
    <meta charset="utf-8">
    <TITLE>Musitray - Sua música a um clique</TITLE>
    <link rel="stylesheet" href="../bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/estilo.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.filterizr-with-jquery.min.js"></script>
    <script src="../js/jquery.filterizr.min.js"></script>
    <script src="../js/imagesloaded.pkgd.js"></script>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-resource.js"></script>
    <script src="../js/vue-router.js"></script>
    <script src="../js/sweetalert.min.js"></script>
</HEAD>

<script  type="text/x-template" id="profissionais-template">
    <div class="col-12 filtr-item"  :data-category="profissional.idInstrumento">
        <div class="m-2 bg-white rounded busca-profissional">
            <div class="row">
                <div class="col-8">
                    <div class="row">
                        <div class="col-4">
                            <img class="card-img-top foto-perfil-100" :src="profissional.foto" alt="Card image cap">
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="card-title">{{profissional.nomeProfissional}}</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="badge badge-primary m-1"
                                        v-for="instrumento in profissional.instrumentos"
                                        :instrumento="instrumento"
                                        >{{instrumento.nome}}
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <p class="card-text">{{profissional.descricao}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="row">
                        <div class="col-12">
                            <button @click="abrePerfil(profissional.idProfissional)" class="btn btn-primary btn-block">Perfil completo</button>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-12">
                            <br><br>
                            <p>Preços</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script  type="text/x-template" id="instrumentos-filtro-template">
        <h2 class="badge badge-primary m-1">
            {{instrumento.label}}
            <label class="ml-2" @click="removeFiltro(instrumento.id)">x</label>
        </h2>
</script>

<BODY>      
<?php 
    include "../cabecalho/cabecalho.php"
?>
<div class="container" id="app">
    <div class="row">
        <div class="col-3" style="position: fixed;">
            <div class="row">
                <div class="col-12">
                    <input class="form-control mr-sm-2" placeholder="Procurar" name="search" data-search>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <b>Instrumentos</b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input type="text" class="form-control" id="inputInstrumento" placeholder="Instrumentos...">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <instrumentos-filtro
                                v-for="instrumento in filtroInstrumentos"
                                v-bind:key="instrumento.id"
                                :instrumento="instrumento"
                            ></instrumentos-filtro>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                </div>
            </div>
        </div>
        <div class="col-8 offset-4 filtr-container">
            <div class="row mb-4">
                <profissionais
                    v-for="profissional in profissionais"
                    v-bind:key="profissional.idProfissional"
                    :profissional="profissional"
                ></profissionais>
            </div>
        </div>
    </div>
</div>

<?php 
    include "../rodape/rodape.php"
?>

<script src="../js/popper.min.js" crossorigin="anonymous"></script>
<script src="encontreProfissionais.js" crossorigin="anonymous"></script>
<script src="../bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 

</BODY>
</HTML>