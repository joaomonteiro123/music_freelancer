<?php
include "../../sessao.php";

$idUsuario = $_SESSION['idUsuario'];
$idUsuario = 0;

$query = "SELECT p.nome_artistico as nome, p.id as id, u.id as id_usuario, u.foto, GROUP_CONCAT(i.nome SEPARATOR ', ') AS instrumento_nome
            , IFNULL(GROUP_CONCAT(i.id SEPARATOR ', '),'0') AS instrumento_id, p.descricao
            FROM profissional p LEFT JOIN profissional_instrumentos pi ON pi.id_profissional = p.id
            LEFT JOIN instrumentos i ON (pi.id_instrumentos = i.id) JOIN usuario u ON u.id = p.id_usuario AND u.id != (?)
            GROUP BY p.id";

$query = $conexao->prepare($query);
$query ->bind_param('i',$idUsuario);
$query->execute();

$res = $query->get_result();

$i = 0;
$profissional = array();
while ($n = $res -> fetch_assoc()) {
    $profissional[$i]['nomeProfissional'] = utf8_encode($n['nome']);
    $profissional[$i]['descricao'] = utf8_encode($n['descricao']);
    $profissional[$i]['idProfissional'] = $n['id'];
    if ( $n['foto'] == "" ){
        $n['foto'] = "../images/generic_user.png";
    }else{
        $n['foto'] = "../servicos/imagemUsuario.php?id=".$n['id_usuario'];;
    }   
    $profissional[$i]['foto'] = $n['foto'];
    $profissional[$i]['idInstrumento'] = $n['instrumento_id'];
    $profissional[$i]['nomeInstrumento'] = utf8_encode($n['instrumento_nome']);

    $profissional[$i]['instrumentos'] = array();
    $instrumentosAux = array();
    $instrumentos = array();
    $instrumentosAux['id'] = explode(', ', $n['instrumento_id']);
    $instrumentosAux['nome'] = explode(', ', $n['instrumento_nome']);
    if($instrumentosAux['nome'][0] != ""){
        for ($j = 0; $j < count($instrumentosAux['id']); $j++){
            $instrumentos[$j]['id'] =  utf8_encode($instrumentosAux['id'][$j]);
            $instrumentos[$j]['nome'] = utf8_encode($instrumentosAux['nome'][$j]);
            $profissional[$i]['instrumentos'] = $instrumentos;
        }
    }
    $i = $i + 1;
}

$conexao->close();
echo json_encode($profissional);
?>