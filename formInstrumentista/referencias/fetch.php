<?php
include "../../sessao.php";
$idProjeto = $_SESSION['idProjeto'];
$query = "SELECT * FROM arquivosprojeto WHERE categoria = 'referencia'
          AND id_projeto = (?) ORDER BY id DESC";
$query = $conexao->prepare($query);
$query->bind_param("i", $idProjeto);
$query->execute();
$resultSet = $query->get_result();
$result = $resultSet->fetch_all();
$number_of_rows = mysqli_num_rows($resultSet);
$conexao->close();
$output = '';

if($number_of_rows > 0)
{
 $count = 0;
 foreach($result as $row)
 {
  $count ++; 
  $output .= '
    <div class="row referencia-file mt-3">
        <div class="col-10">
            <input type="text" class="form-control input disabled" value="'.$row[1].'" disabled>
        </div>
        <div class="col-2 input apaga form-control icone-input-container popover_wrapper delete_referencia" id="'.$row[0].'" data-image_name="'.$row[2].'">
            <svg class="icone-input" version="1.1" id="Capa_1" x="0px" y="0px"
                width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                xml:space="preserve">
                <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
            </svg>
            <div class="popover-content">
                Remover arquivo
            </div>
        </div>
    </div>';
 }
}
else
{
  $output .= '
  <div class="text-center">Não foram adicionados arquivos</div>
';
}
$output .= '</table>';
echo $output;
?>