<?php
include "../sessao.php";

if (isset($_SESSION['idProjeto']) && isset($_SESSION['idProjetoInstrumentista'])){
}else{
    header('Location: ../novoProjeto/novoProjeto.php'); 
}
?>

<HTML>
<HEAD>
<meta charset="utf-8">
    <TITLE>Musitray - Sua música a um clique</TITLE>
    <link rel="stylesheet" href="../bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/estilo.css">
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/combobox.js"></script>
    <script src="../js/jquery.filterizr-with-jquery.min.js"></script>
    <script src="../js/jquery.filterizr.min.js"></script>
    <script src="../js/imagesloaded.pkgd.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-resource.js"></script>
    <script src="../js/sweetalert.min.js"></script>
</HEAD>

<BODY>      
<?php 
    include "../cabecalho/cabecalho.php"
?>

<div class="container">
    <div class="row pt-4">
        <div class="col-12">
            <label class="texto-principal grande">Envie sua demanda</label>
        </div>
    </div>
    <div class="row">
        <div class="col-12" id="div_pages">
            <div id="etapa1">
                <form id="form1" class= "needs-validation">
                    <div class="row">
                        <div class="col-12">
                        <label class="texto-principal medio">Instrumentos</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label class="texto-principal small">Selecione qual ou quais instrumentos você gostaria que fossem utilizados</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <svg class="icone-input-inside" version="1.1" id="Capa_1" x="0px" y="0px"
                                width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                                xml:space="preserve" @click="mostraInstrumentos()">
                                <rect x="33.214" width="3.333" height="73.799"/>
                                <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
                            </svg>
                            <input type="text" class="form-control input" id="inputInstrumento" placeholder="Instrumentos...">
                            <input type="hidden" id="idInstrumento">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12" id="div_intrumentos"></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label class="texto-principal small">Sentiu falta de algum instrumento?</label>
                            <input maxlength="200" type="text" class="form-control input" id="outrosInstrumentos" placeholder="Outros..." name="outrosInstrumentos">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-check small">
                                <input onclick='formInstrumentista.updateProfissionais(this.checked ? 1 : 0);' class="form-check-input" type="checkbox" value="" id="VST" name="VST" checked>
                                <label class="form-check-label" for="VST">Aceito gravações utilizando instrumentos virtuais.
                                    <a href="https://www.youtube.com/watch?v=jFrkXj8Y3As" target="_blank">O que é isso?</a>
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <!-- <div class="container">
                        <div class="modal fade" id="modalVST" role="dialog">
                            <div class="modal-dialog">                              
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <label class="modal-title texto-principal grande">Instrumentos Virtuais</label>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="resp-container">
                                            <iframe class="resp-iframe" src="https://www.youtube.com/embed/jFrkXj8Y3As" frameborder="0" allowfullscreen></iframe>       
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- Modal-End -->
                </form>
            </div>

            <div id="etapa2">
                <form id="form2" class= "needs-validation mt-4">
                    <div class="row">
                        <div class="col-12">
                            <label class="texto-principal medio">Observações e Referências</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label class="texto-principal small">Observações Gerais</label>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-12">
                            <textarea maxlength='500' type="text" class="form-control input" id="observacoes" name="observacoes"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <small class="text-muted">Descreva aqui observações sobre a sua música. Fale sobre o estilo musical, o que espera com os instrumentos que serão criados ou qualquer outra informação relevante.</small> 
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label class="texto-principal small">Observações Específicas</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <small class="text-muted"> Caso julgue importante, descreva de acordo com a minutagem exata da sua música o que gostaria de escutar.</small> 
                        </div>
                    </div>
                    <div class="row text-center mt-2">
                        <div class="col-3 small">
                            <label class="texto-principal small">Minutagem</label>
                        </div>
                        <div class="col-7 small">
                            <label class="texto-principal small">Observação específica de acordo com a minutagem</label>
                        </div>
                    </div>
                    <div id="div_minutagem">
                        <div class="row mt-2 minutagem-linha">
                            <div class="col-3">
                                <input maxlength="20" type="text" class="form-control input" id="minutagemTempo">
                            </div>
                            <div class="col-7">
                                <input maxlength="200" type="text" class="form-control input"  id="minutagemObs">
                            </div>
                            <div class="col-2">
                                <button id="btn_addMinutagem" type="button" class="btn btn-quadrado w-100" style="padding: 12px;">+</button>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <label class="texto-principal small">Andamento</label>
                            <small class="text-muted ml-3">Caso não tenha utilizado um metrônomo, deixe em branco</small> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input maxlength="200" class="form-control input" id="andamento" placeholder="Qual o andamento?" name="andamento">
                                <div class="invalid-feedback">
                                    Informe o andamento!
                                </div>
                            </input>
                        </div>
                    </div> 
                    <div class="row mt-3">
                        <div class="col-12">        
                            <label class="texto-principal medio">Referências</label>
                            <small class="text-muted ml-3">Existe alguma música que você goste da sonoridade e usa como referência?</small> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-12">
                                    <label class="texto-principal small">Arquivos</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <?php 
                                        include "referencias/main.php";
                                    ?>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12">
                                    <p class="font-italic text-center">
                                        Aguarde a conclusão do carregamento dos arquivos para prosseguir
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-12">
                                    <label class="texto-principal small">Link(s)</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <textarea maxlength="1000" type="text" class="form-control input" id="referenciaLink" name="referenciaLink" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <small class="text-muted">Cole os links aqui</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="row">
                <div class="col-6">
                    <form id="form3">
                        <div class="row">
                            <div class="col-12">
                                <label class="texto-principal medio">Cifra, letra e/ou partitura</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <small class="text-muted">Caso tenha, anexe aqui a cifra, letra e/ou partitura da sua música.</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <?php 
                                    include "cifra/main.php";
                                ?>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <p class="font-italic text-center">
                                    Aguarde a conclusão do carregamento dos arquivos para prosseguir
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-6">
                    <form id="form4">
                        <div class="row">
                            <div class="col-12">
                                <label class="texto-principal medio">Anexe a sua música</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <small class="text-muted">Envie sua música em MP3 apenas para que o produtor possa avaliar</small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <?php 
                                    include "musica/main.php";
                                ?>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <p class="font-italic text-center">
                                    Aguarde a conclusão do carregamento dos arquivos para prosseguir
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div id="etapa5">
                <div class="row">
                    <div class="col-12">
                        <label class="texto-principal medio">Selecionar Profissionais</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <small class="text-muted">Seguem abaixo os profissionais compatíveis com a sua demanda.
                        Você pode escolher para para quais deles enviará a proposta.
                        Os interessados em realizar o trabalho entrarão em contato com você!</small>
                    </div>
                </div>                    
                <div id="app">
                    <div id="div_cards" class="filtr-container w-100">
                        <div class="row">
                            <card-profissional
                                v-for="profissional in profissionais"
                                v-bind:key="profissional.idProfissional"
                                @click.native="selecionar(profissional)"
                                :profissional="profissional"
                            ></card-profissional>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: 2rem"></div>
            <div class="floating-footer">
                <div class="row">
                    <div class="col-12">
                        <button class="btn btn-quadrado btn_concluir float-right w-25">Enviar demanda</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
    include "../rodape/rodape.php"
?>

<script src="../js/popper.min.js" crossorigin="anonymous"></script>
<script src="formInstrumentista.js" crossorigin="anonymous"></script>
<script src="../bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
<script src="cardProfissional.js"></script>
</BODY>
</HTML>