Vue.component("card-profissional", {
    props: ["profissional"],
    template: `
    <div :data-category="[profissional.idInstrumento, ativo()]" :data-orcamento="profissional.orcamento" :data-estrelas="profissional.estrela" class="filtr-item col-3">
        <div :id="profissional.idProfissional" class="card-profissional pl-0 pr-0" :class="{ 'card-ativo': profissional.ativo == 1 }" align="center">    
            <div style="overflow:hidden; padding: 0;">
                <img class="img_card_profissional m-4" :src="profissional.foto">
            </div>
            <div class="row">
                <div class="col-12">
                    <h3>{{profissional.nomeProfissional}}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label class="texto-principal small">Instrumentos</label>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label>{{profissional.nomeInstrumento || '-'}}</label>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label class="texto-principal small">Especialidades</label>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label>{{profissional.nomeEspecialidade || '-'}}</label>
                </div>
            </div>
            <hr>
            <button @click="abreProfissional(profissional)" type="button" class="btn btn-quadrado mb-2 btn-info-profissional" style="width: 90%">Perfil</button>
        </div>
    </div>`,
    methods: {
        ativo: function () {
            return this.profissional.ativo == 1 ? 'ativo' : 'nao-ativo';
        },
        abreProfissional: function (profissional) {
            window.open("../perfilProfissional/perfilProfissional.php?id=" + profissional.idProfissional);
        },
    }
})

formInstrumentista = new Vue({
    el: "#app",
    data: {
        profissionais: [],
        profissionalSelecionado: [],
    },
    created: function () {
        this.updateProfissionais();
    },
    methods: {
        //Seleção de cards de profissionais
        selecionar: function (profissional) {
            profissional.ativo == false ? profissional.ativo = true : profissional.ativo = false
        },

        updateProfissionais(vlc = 1) {
            $("body").addClass("loading");
            this.$http.get("servicos/carregaProfissionais.php", { params: { vlc } }).then(function (resposta) {
                this.profissionais = resposta.data;
                $("body").removeClass("loading");
            });
        }
    }
})
