<!DOCTYPE html>
<html>
  <br />
  <div class="container">
   <div class="text-center">
     <button type="button" class="btn btn-quadrado" id="adiciona_musica">Adicionar arquivo</button>
    <input type="file" -
    name="multiple_files_musica" id="multiple_files_musica" class="invisivel" multiple />
  </div>
  <div class="text-center">
    <span id="error_multiple_files_musica"></span>
  </div>
   <br />
   <div id="image_table_musica"> 
   </div>
  </div>
</html>
<script>
$(document).ready(function(){
 load_image_data();
 function load_image_data()
 {
  $.ajax({
   url:"musica/fetch.php",
   data:"idProjeto="+$("#projetoID").val(),
   method:"POST",
   success:function(data)
   {
    $('#image_table_musica').html(data);
   }
  });
 };
 
 $('#adiciona_musica').click(() => {
   $('#multiple_files_musica').trigger("click");
 });

 $('#multiple_files_musica').change(function(){
  var error_images = '';
  var form_data = new FormData();
  var files = $('#multiple_files_musica')[0].files;
  if($('.musica-file').length >= 3)
  {
   error_images += 'Você não pode selecionar mais do que 3 arquivos';
  }
  else
  {
   for(var i=0; i<files.length; i++)
   {
    var name = document.getElementById("multiple_files_musica").files[i].name;
    var ext = name.split('.').pop().toLowerCase();
    if(jQuery.inArray(ext, ['mp3', 'mp4']) == -1) 
    {
      error_images += '<p>Arquivos '+ext+' são inválidos</p>';
    }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("multiple_files_musica").files[i]);
    var f = document.getElementById("multiple_files_musica").files[i];
    var fsize = f.size||f.fileSize;
    if(fsize > 15000000)
    {
     error_images += '<p>O arquivo é muito grande. O máximo aceito é 15 Mb</p>';
    }
    else
    {
     form_data.append("file[]", document.getElementById('multiple_files_musica').files[i]);
    }
   }
  }
  if(error_images == '') 
  {
   $.ajax({
    url:"musica/upload.php",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
     $('#error_multiple_files_musica').html('<br /><label class="text-primary">Enviando...</label>');
    },   
    success:function(data)
    {
     $('#error_multiple_files_musica').html('');
     load_image_data();
    }
   });
  }
  else
  {
   $('#multiple_files_musica').val('');
   $('#error_multiple_files_musica').html("<span class='text-danger'>"+error_images+"</span>");
   return false;
  }
 });   
 $(document).on('click', '.delete_musica', function(){
  var image_id = $(this).attr("id");
  var image_name = $(this).data("image_name");
   $.ajax({
    url:"musica/delete.php",
    method:"POST",
    data:{image_id:image_id, image_name:image_name},
    success:function(data)
    {
     load_image_data();
    }
   });
 }); 
});
</script>
