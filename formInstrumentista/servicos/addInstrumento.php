<?php 
include "../../sessao.php";

$instrumento = $_POST['idInstrumento'];
$projeto = $_SESSION['idProjetoInstrumentista'];

$query = "INSERT INTO `projeto_instrumentista_intrumentos`
        (`id`, `id_projeto_instrumentista`, `id_instrumentos`)
        VALUES (NULL, (?), (?));";
$query = $conexao->prepare($query);
$query->bind_param("ii", $projeto, $instrumento);

if ($query->execute() === TRUE) {
    $id = $conexao->insert_id;
} else {
    echo "erro";
}
$conexao->close();

echo $id;
?>