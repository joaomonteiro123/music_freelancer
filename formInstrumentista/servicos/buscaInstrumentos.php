<?php
include "../../sessao.php";

$_GET['term'] = "%".$_GET['term']."%";
$_GET['term'] = trim(strip_tags(utf8_decode($_GET['term'])));
$projeto = $_SESSION['idProjetoInstrumentista'];
$query = "SELECT * FROM `instrumentos`
            WHERE lower(nome) LIKE  lower((?))
            AND id NOT IN (SELECT id_instrumentos
                            FROM projeto_instrumentista_intrumentos
                            WHERE id_projeto_instrumentista = (?))
            ORDER BY NOME";
$query = $conexao->prepare($query);

$query->bind_param("si", $_GET['term'], $projeto);

$query->execute();
$res = $query->get_result();
$nomes = array();

$i = 0;
while ($n = $res -> fetch_assoc()) {
    $nomes[$i]["id"] = utf8_encode($n['id']);
    $nomes[$i]["label"] = utf8_encode($n['nome']);
    $nomes[$i]["value"] = utf8_encode($n['nome']);
    $i = $i + 1;
}
$conexao->close();
echo json_encode($nomes);
?>