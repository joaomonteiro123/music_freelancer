<?php
include "../../sessao.php";

$id = $_POST['id'];

$query = "DELETE FROM projeto_instrumentista_intrumentos
            WHERE id_projeto_instrumentista = (?)
            AND id_instrumentos = (?)";
$query = $conexao->prepare($query);
$query->bind_param("ii", $_SESSION['idProjetoInstrumentista'], $id);
$query->execute();

$conexao->close();
?>