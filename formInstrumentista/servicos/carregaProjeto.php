<?php
include "../../sessao.php";

$id = $_SESSION['idProjetoInstrumentista'];

$query = "SELECT p.observacoes_gerais, p.metronomo, p.referencia_link, p.minutagem,
p.outros_instrumentos, p.fl_VST FROM projeto_instrumentista p WHERE p.id = (?)";

$query = $conexao->prepare($query);
$query -> bind_param('i',$id);
if ($query->execute() === TRUE) {
} else {
    echo "erro";
}

$res = $query->get_result();

$i = 0;
$projeto = array();
while ($n = $res -> fetch_assoc()) {
    $projeto[$i]['observacoes'] = utf8_encode($n['observacoes_gerais']);
    $projeto[$i]['metronomo'] = $n['metronomo'];
    $projeto[$i]['referencia'] = utf8_encode($n['referencia_link']);
    $projeto[$i]['outrosInstrumentos'] = utf8_encode($n['outros_instrumentos']);
    $projeto[$i]['VST'] = $n['fl_VST'];
    $projeto[$i]['minutagem'] = utf8_encode($n['minutagem']);   
    $i = $i + 1;
}

$conexao->close();
echo json_encode($projeto);
?>