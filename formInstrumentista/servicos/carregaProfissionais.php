<?php
include "../../sessao.php";

$idUsuario = $_SESSION['idUsuario'];
$idProjetoInstrumentista = $_SESSION['idProjetoInstrumentista'];

$query = "SELECT p.nome_artistico as nome, p.id as id, u.foto, u.id as id_usuario, GROUP_CONCAT(i.nome SEPARATOR ', ') AS instrumento_nome
            , GROUP_CONCAT(i.id SEPARATOR ', ') AS instrumento_id, GROUP_CONCAT(e.descricao SEPARATOR ', ') AS especialidade_nome
            , GROUP_CONCAT(e.id SEPARATOR ', ') AS especialidade_id, p.descricao
            FROM profissional p
            LEFT JOIN profissional_instrumentos pi ON pi.id_profissional = p.id
            LEFT JOIN instrumentos i ON (pi.id_instrumentos = i.id)
            LEFT JOIN profissional_especialidades pe ON pe.id_profissional = p.id
            LEFT JOIN especialidades e ON (pe.id_especialidades = e.id)
            JOIN usuario u ON u.id = p.id_usuario AND u.id != (?)
            WHERE
                    pi.id_instrumentos in (SELECT id_instrumentos FROM projeto_instrumentista_intrumentos pii WHERE pii.id_projeto_instrumentista = (?))
                    OR (p.vlc = 1 AND (?) = 1)
            GROUP BY p.id";

$query = $conexao->prepare($query);
$query ->bind_param('iii', $idUsuario, $idProjetoInstrumentista, $_GET['vlc']);
$query->execute();

$res = $query->get_result();
$j=0;

$i = 0;
$profissional = array();
while ($n = $res -> fetch_assoc()) {
    $j=$j+0.7;
    $estrela=2.0+$j;
    $orcamento = 1500 - 300 * $j;
    $orcamento = number_format($orcamento, 2, ',', '.');
    $profissional[$i]['nomeProfissional'] = utf8_encode($n['nome']);
    $profissional[$i]['descricao'] = utf8_encode($n['descricao']);
    $profissional[$i]['idProfissional'] = $n['id'];
    $profissional[$i]['idInstrumento'] = $n['instrumento_id'];
    $profissional[$i]['nomeInstrumento'] = utf8_encode($n['instrumento_nome']);
    $profissional[$i]['idEspecialidade'] = $n['especialidade_id'];
    $profissional[$i]['nomeEspecialidade'] = utf8_encode($n['especialidade_nome']);
    $profissional[$i]['estrela'] = $estrela;
    $profissional[$i]['orcamento'] = $orcamento;
    $profissional[$i]['ativo'] = false;

    if ( $n['foto'] == "" ){
        $n['foto'] = "../images/generic_user.png";
    }else{
        $n['foto'] = "../servicos/imagemUsuario.php?id=".$n['id_usuario'];
    }   
    $profissional[$i]['foto'] = $n['foto'];
    
    $i = $i + 1;
}

$conexao->close();
echo json_encode($profissional);
?>