<?php 
include "../../sessao.php";

$idProjetoInstrumentista = $_SESSION['idProjetoInstrumentista'];
$idProjeto = $_SESSION['idProjeto'];
$profissionais = $_POST['profissionaisAtivos'];
$instrumentos = UTF8_DECODE($_POST['outrosInstrumentos']);
if(isset($_POST['VST'])){
    $vst = 1;
}else{
    $vst = 0;
}
$observacoes = UTF8_DECODE($_POST['observacoes']);
$andamento = $_POST['andamento'];
$referenciaLink = UTF8_DECODE($_POST['referenciaLink']);

$minutagem = array();
$a = 0;
for ($i = 0; $i <= $_POST['minutagem']; $i++){
    if( isset($_POST['minutagemTempo'.$i])){
        $minutagem['tempo'][$a] = $_POST['minutagemTempo'.$i];
        $minutagem['obs'][$a] = $_POST['minutagemObs'.$i];
        $a = $a + 1;
    }
}
$minutagem = json_encode($minutagem);

$query = "UPDATE projeto_instrumentista
            SET observacoes_gerais = (?), metronomo = (?),
            referencia_link = (?), minutagem = (?), outros_instrumentos = (?),
            fl_VST = (?) WHERE id = (?)";

if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query->bind_param("sisssii", $observacoes, $andamento, $referenciaLink, $minutagem, $instrumentos, $vst, $idProjetoInstrumentista)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
    $conexao->close();
}


$profissionais = explode(',', $profissionais);
foreach ($profissionais as $profissional) {
    $query = "INSERT INTO `projeto_envio` (`id`, `id_profissional`, `id_projeto`) VALUES (NULL, (?), (?));";
    if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));
    }
    if (!$query->bind_param("ii", $profissional, $idProjeto)){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
    }
    if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
    }
}

$conexao->close();
echo $idProjeto;
?>