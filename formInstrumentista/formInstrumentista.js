$(document).ready(function () {
    //Modal
    $('#modalVirtual').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })
    $("#modalVirtual").on('hidden.bs.modal', function (e) {
        $("#modalVirtual iframe").attr("src", $("#modalVirtual iframe").attr("src"));
    });

    //Autocomplete
    $("#inputInstrumento").autocomplete({
        source: "servicos/buscaInstrumentos.php",
        minLength: 0,
        select: function (event, ui) {
            $("#idInstrumento").val(ui.item.id);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                swal("Atenção", "Selecione um instrumento válido", "info");
                $("#inputInstrumento").val("");
                $("#inputInstrumento").focus();
                $("#idInstrumento").val(0);
            }
        }
    });

    $("#inputInstrumento").click(function () {
        $("#inputInstrumento").autocomplete("search", "");
    });
    $("#inputInstrumento").on("autocompleteselect", function (event, ui) {
        adicionaInstrumento(ui.item.label, ui.item.id);
    });

    //Adiciona instrumentos
    function adicionaInstrumento(nome, id) {
        if (id != 0) {
            $.ajax({
                url: "servicos/addInstrumento.php",
                method: "POST",
                data: "idInstrumento=" + id,
                success: function (data) {
                    if (data == "erro") {
                        swal("Erro", "Um erro inesperado ocorreu", "error");
                    }
                    else {
                        formInstrumentista.updateProfissionais($("#VST").prop('checked') ? 1 : 0);
                        let newContent = `
                            <div class="row btn_instrumento mt-3">
                                <div class="col-10">
                                    <input id="id-instrumento-adicionado-${id}" type="text" class="form-control input disabled" value=${nome} disabled>
                                </div>
                                <div class="col-2 input apaga form-control icone-input-container popover_wrapper rmv_instrumento" id_instrumento=${id}>
                                    <svg class="icone-input" version="1.1" id="Capa_1" x="0px" y="0px"
                                        width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                                        xml:space="preserve">
                                        <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
                                    </svg>
                                    <div class="popover-content">
                                        Remover ${nome}
                                    </div>
                                </div>
                            </div>`;
                        $("#div_intrumentos").append(newContent);
                        $(`#id-instrumento-adicionado-${id}`).val(nome);

                        $("#inputInstrumento").val("");
                        $("#inputInstrumento").focus();
                        $("#idInstrumento").val(0);

                        //Remove instrumentos
                        $(".rmv_instrumento").click(function () {
                            var instrumento = $(this).parent();
                            var idInstrumento = $(this).attr('id_instrumento');

                            $.ajax({
                                url: "servicos/removeInstrumento.php",
                                method: "POST",
                                data: "id=" + idInstrumento,
                                success: function () {
                                    formInstrumentista.updateProfissionais($("#VST").prop('checked') ? 1 : 0);
                                    instrumento.remove();
                                }
                            })
                        });
                    }
                }
            });
        }
    }

    //Quantidade dinâmica de informações por minutagem

    $("#btn_addMinutagem").click(function () {
        adicionaMinutagem();
    });
    minutagem = 0;
    function adicionaMinutagem() {
        minutagem = minutagem + 1;
        let newRow = `
            <div class="row mt-2">
                <div class="col-3">
                    <input type="text" class="form-control input disabled" name="minutagemTempo${minutagem}" value="${$("#minutagemTempo").val()}" disabled>
                </div>
                <div class="col-7">
                    <input type="text" class="form-control input disabled"  name="minutagemObs${minutagem}" value="${$("#minutagemObs").val()}" disabled>
                </div>
                <div class="col-2 input apaga form-control icone-input-container popover_wrapper delete-minutagem">
                    <svg class="icone-input" version="1.1" id="Capa_1" x="0px" y="0px"
                        width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                        xml:space="preserve">
                        <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
                    </svg>
                    <div class="popover-content">
                        Remover minutagem
                    </div>
                </div>
            </div>`

        $("#div_minutagem").append(newRow);
        $(".delete-minutagem").click(function () {
            $(this).parent().remove();
        });

        $("#minutagemTempo").val("");
        $("#minutagemObs").val("");
    }

    $(".btn_concluir").click(function () {
        swal({
            title: "Tem certeza?",
            text: "Deseja finalizar e enviar a demanda?",
            icon: "warning",
            buttons: {
                cancel: "Continuar editando",
                criar: {
                    text: "Criar demanda!",
                    value: "criar",
                }
            },
        })
            .then((value) => {
                if (value == 'criar') {
                    profissionaisAtivos = [];
                    i = 0;
                    $(".card-profissional.card-ativo").each(function () {
                        profissionaisAtivos[i] = $(this).attr('id');
                        i = i + 1;
                    });
                    $.ajax({
                        type: "post",
                        url: "servicos/atualizaProjeto.php",
                        data: $("#form1, #form2").serialize() + "&minutagem=" + $('.minutagem-linha').length + "&profissionaisAtivos=" + profissionaisAtivos,
                        success: function (resposta) {
                            if (resposta == "erro") {
                                swal("Erro", "Um erro inesperado ocorreu", "error");
                            } else {
                                window.location = '../abreProjeto/abreProjeto.php?id=' + resposta;
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal("Erro", "Um erro inesperado ocorreu", "error");
                        }
                    });
                }
            });
    });
});