<meta charset="utf-8">

<footer class="footer">
    <div class="footer-content">
        <a class="float-middle" href="../home/home.php">
            <img class="d-block mx-auto p-1" id="img_home" src="../images/logo.png" height="50px">
        </a>
        <div class="text text-center">
            <p class="float-middle text-muted">Todos os direitos reservados | musitray@gmail.com</p>
        </div>
    </div>
</footer>

<!-- <script src="../rodape/rodape.js" crossorigin="anonymous"></script> -->