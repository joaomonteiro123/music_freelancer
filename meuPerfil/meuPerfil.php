<?php
include "../sessao.php";
?>

<HTML>
<HEAD>
<meta charset="utf-8">
    <TITLE>Musitray - Sua música a um clique</TITLE>
    <link rel="stylesheet" href="../bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/estilo.css">
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/combobox.js"></script>
    <script src="../js/jquery.filterizr-with-jquery.min.js"></script>
    <script src="../js/jquery.filterizr.min.js"></script>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-resource.js"></script>
    <script src="../js/vue-router.js"></script>
    <script src="../js/sweetalert.min.js"></script>
</HEAD>

<script  type="text/x-template" id="tabela-template">
    <tr :id="projeto.id">
        <td>{{projeto.nome}} <span v-if="projeto.notificacao > 0" class="badge badge-pill badge-danger">{{projeto.notificacao}}</span></td>
        <td>{{projeto.data}}</td>
        <td><button type="button" class="btn btn-quadrado" @click="abreProjeto(projeto.id)">Abrir</button></td>
        <td><button type="button" class="btn btn-quadrado cancelar"  @click="apagaProjeto(projeto.id, projeto.aceito)">Apagar</button></td>
    </tr>
</script>

<script  type="text/x-template" id="tabela-profissional-template">
    <tr :id="projeto.id">
        <td>{{projeto.nome}} <span v-if="projeto.notificacao > 0" class="badge badge-pill badge-danger">{{projeto.notificacao}}</span></td>
        <td>{{projeto.data}}</td>
        <td><button type="button" class="btn btn-quadrado" @click="abreProjeto(projeto.id)">Abrir</button></td>
    </tr>
</script>

<script  type="text/x-template" id="modal-apaga">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">{{ titulo }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" @click="cancela()">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>{{ mensagem }}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger cancel" @click="cancela()">Cancelar</button>
                            <button type="button" class="btn btn-outline-primary confirm" @click="confirma()">{{botao}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </transition>
</script>


<script  type="text/x-template" id="projeto-portfolio-template">
    <div class="col-8 offset-2">
        <div class="card" v-if="projeto.tipo == 'youtube'">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="resp-iframe" :src="linkYouTube(projeto.link)"
                frameborder="0" allowfullscreen>
                </iframe>  
            </div>
            <div class="card-body">
                <h5 class="card-title">{{projeto.nome}}</h5>
                <p class="card-text">{{projeto.descricao}}</p>
            </div>
        </div>
        
        <div class="card" v-if="projeto.tipo == 'spotify'">
            <div v-html="linkSpotify(projeto.link)">
            </div>
        </div>
    </div>
</script>

<script type="text/x-template" id="portfolio-template"> 
    <div v-if="projeto.tipo == 'youtube'" class="col-4 d-flex mb-3" style="padding-left: 0px;">
        <div class="card w-100">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="resp-iframe" :src="linkYouTube(projeto.link)"
                    frameborder="0" allowfullscreen>
                </iframe>  
            </div>
            <div class="card-body portfolio-wrapper">
                <div class="nome-portifolio">{{projeto.nome}}</div>
                <div class="decricao-campo">{{projeto.descricao}}</div>
            </div>
            <div class="card-footer">
                <button type="button" class="btn btn-block btn-quadrado"
                @click="abreProjeto(projeto)"
                href="#modalProjetoPortfolio" data-target="#modalProjetoPortfolio" data-toggle="modal"
                >Editar</button>
            </div>
        </div>
    </div>

    <div v-else-if="projeto.tipo == 'spotify'" class="col-4 d-flex mb-3" style="padding-left: 0px;">
        <div class="card w-100">
            <div v-if="projeto.tipo == 'spotify'">
                <div v-html="linkSpotify(projeto.link)">
                </div>
            </div>
            <div class="card-footer">
                <button type="button" class="btn btn-block btn-quadrado"
                @click="abreProjeto(projeto)"
                href="#modalProjetoPortfolio" data-target="#modalProjetoPortfolio" data-toggle="modal"
                >Editar</button>
            </div>
        </div>
    </div>

</script>

<script  type="text/x-template" id="instrumentos-template">
    <div class="row">
        <div class="col-12">
            <label>{{ instrumentos.nome }}</label>
        </div>
    </div>
</script>

<script  type="text/x-template" id="especialidades-template">
    <div class="row">
        <div class="col-12">
            <label>{{ especialidade.descricao }}</label>
        </div>
    </div>
</script>

<script  type="text/x-template" id="instrumentos-apaga-template">
    <div class="row" style="margin-bottom: 0.5vh; padding-right: 4%;">
        <div class="col-10" style="padding-right: 0.5vw;">
            <input type="text" class="form-control input disabled" :value="instrumentos.nome" disabled>
        </div>
        <div class="col-2 input apaga form-control icone-input-container popover_wrapper" @click="apagaInstrumento(instrumentos.id)">
            <svg class="icone-input" version="1.1" id="Capa_1" x="0px" y="0px"
                width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                xml:space="preserve">
                <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
            </svg>
            <div class="popover-content">
                Remover {{instrumentos.nome}}
            </div>
        </div>
    </div>
</script>

<script  type="text/x-template" id="especialidades-apaga-template">
    <div class="row" style="margin-bottom: 0.5vh; padding-right: 4%;">
        <div class="col-10" style="padding-right: 0.5vw;">
            <input type="text" class="form-control input disabled" :value="especialidade.descricao" disabled>
        </div>
        <div class="col-2 input apaga form-control icone-input-container popover_wrapper" @click="apagaEspecialidade(especialidade.id)">
            <svg class="icone-input" version="1.1" id="Capa_1" x="0px" y="0px"
                width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                xml:space="preserve">
                <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
            </svg>
            <div class="popover-content">
                Remover {{especialidade.descricao}}
            </div>
        </div>
    </div>
</script>

<script  type="text/x-template" id="seleciona-intrumento-temaplate">
    <div class="row">
        <div class="col-12">
            <svg class="icone-input-inside" version="1.1" id="Capa_1" x="0px" y="0px"
                width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                xml:space="preserve" @click="mostraInstrumentos()">
                <rect x="33.214" width="3.333" height="73.799"/>
                <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
            </svg>
            <input @click="mostraInstrumentos()" type="text" class="form-control input" id="inputInstrumento" placeholder="Instrumentos...">
        </div>
    </div>
</script>

<script  type="text/x-template" id="seleciona-especialidades-temaplate">
    <div class="row">
        <div class="col-12">
            <svg class="icone-input-inside" version="1.1" id="Capa_1" x="0px" y="0px"
                width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                xml:space="preserve" @click="mostraEspecialidades()">
                <rect x="33.214" width="3.333" height="73.799"/>
                <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
            </svg>
            <input @click="mostraEspecialidades()" type="text" class="form-control input" id="inputEspecialidade" placeholder="Especialidades...">
        </div>
    </div>
</script>

<BODY>   
<?php 
    include "../cabecalho/cabecalho.php"
?>
<div class="container" id="app">
    <div class="menu-lateral-perfil bg-light">
        <div class="row">
            <div class="col-12">
                <div class="foto-perfil">
                    <img :src="usuario.foto">
                </div>
                <input id="input-foto" class="foto-input" type="file" @change="alteraFoto($event)">
                <span @click='clickInputFoto()' class="icone-edita-foto">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            width="60%" height="60%" viewBox="0 0 87.542 87.33" enable-background="new 0 0 87.542 87.33" xml:space="preserve">
                        <g>
                            <path d="M81.873,5.667c-7.537-7.555-19.791-7.555-27.326,0L4.445,55.748c-0.393,0.392-0.635,0.896-0.709,1.436L0.024,84.679
                                c-0.112,0.783,0.169,1.565,0.709,2.106c0.466,0.467,1.119,0.747,1.771,0.747c0.113,0,0.225,0,0.336-0.019l16.564-2.238
                                c1.38-0.188,2.351-1.455,2.163-2.836c-0.185-1.38-1.455-2.351-2.835-2.163L5.451,82.067l2.592-19.175l20.183,20.182
                                c0.466,0.465,1.119,0.746,1.772,0.746c0.652,0,1.305-0.261,1.772-0.746l50.102-50.082c3.654-3.657,5.669-8.506,5.669-13.673
                                C87.542,14.153,85.527,9.303,81.873,5.667z M55.515,11.822l8.413,8.412L18.21,65.952l-8.412-8.413L55.515,11.822z M30.018,77.74
                                l-8.226-8.228l45.717-45.716l8.228,8.226L30.018,77.74z M79.241,28.422L59.116,8.296c2.556-2.108,5.744-3.264,9.103-3.264
                                c3.823,0,7.405,1.492,10.109,4.178c2.704,2.685,4.178,6.285,4.178,10.109C82.506,22.696,81.349,25.867,79.241,28.422z"/>
                        </g>
                    </svg>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                {{uploadFotoMessage}}
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-12">
                <label class="texto-preto medio"><b>{{usuario.primeiroNome}}</b></label>
            </div>
        </div>
        <div class="opcao-menu-lateral" @click="mudaAba('projetos')" :class="abaAtiva.codigo == 'projetos' ? 'ativo' : ''">
            <svg class="icone-menu-lateral" version="1.1" id="Capa_1" x="0px" y="0px"
                    width="22px" height="16.32px" viewBox="11.219 0 96.201 71.497" enable-background="new 11.219 0 96.201 71.497"
                    xml:space="preserve">
                <g>
                    <path d="M104.29,63.909H37.482c-1.727,0-3.131,1.403-3.131,3.131c0,1.727,1.405,3.132,3.131,3.132h66.809
                        c1.726,0,3.13-1.405,3.13-3.132C107.42,65.313,106.015,63.909,104.29,63.909z"/>
                    <path d="M104.29,32.617H37.482c-1.727,0-3.131,1.405-3.131,3.131c0,1.727,1.405,3.131,3.131,3.131h66.809
                        c0.836,0,1.623-0.326,2.214-0.917c0.591-0.591,0.916-1.377,0.916-2.213C107.42,34.021,106.015,32.617,104.29,32.617z"/>
                    <path d="M104.29,1.325H37.482c-1.727,0-3.131,1.404-3.131,3.131s1.405,3.132,3.131,3.132h66.809c1.726,0,3.13-1.405,3.13-3.132
                        S106.015,1.325,104.29,1.325z"/>
                    <path d="M16.061,0c-2.669,0-4.841,2.172-4.841,4.841s2.172,4.842,4.841,4.842s4.841-2.172,4.841-4.842S18.73,0,16.061,0z"/>
                    <path d="M16.061,30.906c-2.669,0-4.841,2.172-4.841,4.842s2.172,4.841,4.841,4.841s4.841-2.171,4.841-4.841
                        S18.73,30.906,16.061,30.906z"/>
                    <path d="M16.061,61.813c-2.669,0-4.841,2.172-4.841,4.842c0,2.669,2.172,4.842,4.841,4.842s4.841-2.173,4.841-4.842
                        C20.902,63.985,18.73,61.813,16.061,61.813z"/>
                </g>
            </svg>
            Meus Projetos
        </div>
        <div class="opcao-menu-lateral" @click="mudaAba('editaConta')" :class="abaAtiva.codigo == 'editaConta' ? 'ativo' : ''">
            <svg class="icone-menu-lateral" version="1.1" id="Capa_1" x="0px" y="0px"
                    width="22px" height="16.32px" viewBox="11.219 0 96.201 71.497" enable-background="new 11.219 0 96.201 71.497"
                    xml:space="preserve">
                <g>
                    <path d="M104.29,63.909H37.482c-1.727,0-3.131,1.403-3.131,3.131c0,1.727,1.405,3.132,3.131,3.132h66.809
                        c1.726,0,3.13-1.405,3.13-3.132C107.42,65.313,106.015,63.909,104.29,63.909z"/>
                    <path d="M104.29,32.617H37.482c-1.727,0-3.131,1.405-3.131,3.131c0,1.727,1.405,3.131,3.131,3.131h66.809
                        c0.836,0,1.623-0.326,2.214-0.917c0.591-0.591,0.916-1.377,0.916-2.213C107.42,34.021,106.015,32.617,104.29,32.617z"/>
                    <path d="M104.29,1.325H37.482c-1.727,0-3.131,1.404-3.131,3.131s1.405,3.132,3.131,3.132h66.809c1.726,0,3.13-1.405,3.13-3.132
                        S106.015,1.325,104.29,1.325z"/>
                    <path d="M16.061,0c-2.669,0-4.841,2.172-4.841,4.841s2.172,4.842,4.841,4.842s4.841-2.172,4.841-4.842S18.73,0,16.061,0z"/>
                    <path d="M16.061,30.906c-2.669,0-4.841,2.172-4.841,4.842s2.172,4.841,4.841,4.841s4.841-2.171,4.841-4.841
                        S18.73,30.906,16.061,30.906z"/>
                    <path d="M16.061,61.813c-2.669,0-4.841,2.172-4.841,4.842c0,2.669,2.172,4.842,4.841,4.842s4.841-2.173,4.841-4.842
                        C20.902,63.985,18.73,61.813,16.061,61.813z"/>
                </g>
            </svg>
            Informações Pessoais
        </div>
        <div class="opcao-menu-lateral" @click="mudaAba('perfilProfissional')" :class="abaAtiva.codigo == 'perfilProfissional' ? 'ativo' : ''">
            <svg class="icone-menu-lateral" version="1.1" id="Capa_1" x="0px" y="0px"
                    width="22px" height="22px" viewBox="11.219 0 96.61 96.123" enable-background="new 11.219 0 96.61 96.123"
                    xml:space="preserve">
                <g>
                    <path d="M100.385,60.323c-1.328,0-2.405,1.076-2.405,2.405v21.362c-0.004,3.986-3.233,7.215-7.218,7.219H23.25
                        c-3.986-0.004-7.215-3.232-7.219-7.219V21.392c0.004-3.984,3.232-7.213,7.219-7.218h21.363c1.329,0,2.407-1.077,2.407-2.406
                        c0-1.329-1.078-2.406-2.407-2.406H23.25c-6.642,0.007-12.023,5.389-12.031,12.03v62.701c0.008,6.642,5.39,12.023,12.031,12.03
                        h67.512c6.642-0.007,12.022-5.389,12.03-12.03V62.729C102.792,61.399,101.715,60.323,100.385,60.323z"/>
                    <path d="M101.838,3.171c-4.229-4.229-11.083-4.229-15.312,0L43.602,46.096c-0.295,0.293-0.507,0.659-0.618,1.059l-5.645,20.38
                        c-0.233,0.835,0.003,1.73,0.617,2.345c0.613,0.611,1.509,0.847,2.344,0.617l20.378-5.646c0.4-0.11,0.765-0.323,1.061-0.617
                        l42.924-42.927c4.223-4.231,4.223-11.081,0-15.313L101.838,3.171z M48.843,47.661l35.132-35.132l11.33,11.33L60.172,58.991
                        L48.843,47.661z M46.581,52.202l9.052,9.052l-12.52,3.47L46.581,52.202z M101.259,17.904l-2.551,2.551L87.376,9.125l2.553-2.552
                        c2.35-2.349,6.157-2.349,8.507,0l2.823,2.824C103.606,11.748,103.606,15.554,101.259,17.904z"/>
                </g>
            </svg>
            Perfil profissional
        </div>
        <div v-if="profissional" class="opcao-menu-lateral" @click="mudaAba('editaPortfolio')" :class="abaAtiva.codigo == 'editaPortfolio' ? 'ativo' : ''">
            <svg class="icone-menu-lateral" version="1.1" id="Capa_1" x="0px" y="0px"
                    width="22px" height="22px" viewBox="11.219 0 96.61 96.123" enable-background="new 11.219 0 96.61 96.123"
                    xml:space="preserve">
                <g>
                    <path d="M100.385,60.323c-1.328,0-2.405,1.076-2.405,2.405v21.362c-0.004,3.986-3.233,7.215-7.218,7.219H23.25
                        c-3.986-0.004-7.215-3.232-7.219-7.219V21.392c0.004-3.984,3.232-7.213,7.219-7.218h21.363c1.329,0,2.407-1.077,2.407-2.406
                        c0-1.329-1.078-2.406-2.407-2.406H23.25c-6.642,0.007-12.023,5.389-12.031,12.03v62.701c0.008,6.642,5.39,12.023,12.031,12.03
                        h67.512c6.642-0.007,12.022-5.389,12.03-12.03V62.729C102.792,61.399,101.715,60.323,100.385,60.323z"/>
                    <path d="M101.838,3.171c-4.229-4.229-11.083-4.229-15.312,0L43.602,46.096c-0.295,0.293-0.507,0.659-0.618,1.059l-5.645,20.38
                        c-0.233,0.835,0.003,1.73,0.617,2.345c0.613,0.611,1.509,0.847,2.344,0.617l20.378-5.646c0.4-0.11,0.765-0.323,1.061-0.617
                        l42.924-42.927c4.223-4.231,4.223-11.081,0-15.313L101.838,3.171z M48.843,47.661l35.132-35.132l11.33,11.33L60.172,58.991
                        L48.843,47.661z M46.581,52.202l9.052,9.052l-12.52,3.47L46.581,52.202z M101.259,17.904l-2.551,2.551L87.376,9.125l2.553-2.552
                        c2.35-2.349,6.157-2.349,8.507,0l2.823,2.824C103.606,11.748,103.606,15.554,101.259,17.904z"/>
                </g>
            </svg>
            Portfólio
        </div>
    </div>

    <div class="row pt-4">
        <div class="col-8 offset-3" style="padding-left: 0px;">
            <div class="row">
                <div class="col-12">
                    <label class="texto-principal grande">{{abaAtiva.nome}}</label>
                </div>
            </div>

            <div v-if="abaAtiva.codigo == 'editaConta'">
                <div class="row">
                    <div class="col-8">
                        <small class="text-muted">Essas informações não serão exibidas para outras pessoas</small>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-6" style="padding-left: 0px; padding-right: 10px;">
                        <label class="texto-principal small">Nome</label>
                    </div>
                    <div class="col-6" style="padding-left: 0px; padding-right: 0;">
                        <label class="texto-principal small">E-mail</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6" style="padding-left: 0px; padding-right: 10px;">
                        <input type="text" class="form-control input" :value="usuario.primeiroNome +' '+ usuario.ultimoNome" disabled>
                    </div>
                    <div class="col-6" style="padding-left: 0px; padding-right: 0px;">
                        <input type="text" class="form-control input" :value="usuario.email" disabled>
                    </div>
                </div>
                <!-- <div class="row mt-2">
                    <div class="col-6" style="padding-left: 0px; padding-right: 10px;">
                        <label class="texto-principal small">Celular</label>
                    </div>
                    <div class="col-6" style="padding-left: 0px; padding-right: 0px;">
                        <label class="texto-principal small">RG</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6" style="padding-left: 0px; padding-right: 10px;">
                        <input type="text" class="form-control input" v-model="usuario.telefone_celular">
                    </div>
                    <div class="col-6" style="padding-left: 0px; padding-right: 0px;">
                        <input type="text" class="form-control input" v-model="usuario.rg">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-6" style="padding-left: 0px; padding-right: 10px;">
                        <label class="texto-principal small">CPF</label>
                    </div>
                    <div class="col-6" style="padding-left: 0px; padding-right: 0px;">
                        <label class="texto-principal small">Profissão</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6" style="padding-left: 0px; padding-right: 10px;">
                        <input type="text" class="form-control input" v-model="usuario.cpf">
                    </div>
                    <div class="col-6" style="padding-left: 0px; padding-right: 0px;">
                        <input type="text" class="form-control input" v-model="usuario.profissao">
                    </div>
                </div>
                <div class="row mt-4 pt-4">
                    <div class="col-12" style="padding-left: 0px; padding-right: 10px;">
                        <button type="button" class="btn btn-quadrado float-right" @click="salvaEdicaoBasico()">Salvar alterações</button>
                        <button type="button" class="btn btn-quadrado cancelar float-right mr-2" @click="cancelaEdicao()">Cancelar</button>
                    </div>
                </div> -->
            </div>

            <div v-else-if="abaAtiva.codigo == 'projetos'">
                <div style="margin-left: -10px; padding: 10px; margin-right: -15px;">
                    <div class="row">
                        <div class="col-12">
                            <div class="row pb-3">
                                <div class="col-12">
                                    <label class="texto-principal small" :class="abaAtivaProjetos == 'todos' || abaAtivaProjetos == 'emAndamento' || abaAtivaProjetos == 'aguardandoProfissional' ? '' : 'inativo'">Como cliente</label>
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="opcao-menu-meus-projetos" @click="mudaAbaProjetos('todos')" :class="abaAtivaProjetos == 'todos' ? 'ativo' : ''">
                                                Todos <span v-if="notificacoes.todos > 0" class="badge badge-pill badge-danger">{{notificacoes.todos}}</span>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="opcao-menu-meus-projetos" @click="mudaAbaProjetos('emAndamento')" :class="abaAtivaProjetos == 'emAndamento' ? 'ativo' : ''">
                                                Em andamento <span v-if="notificacoes.andamento > 0" class="badge badge-pill badge-danger">{{notificacoes.andamento}}</span>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="opcao-menu-meus-projetos" @click="mudaAbaProjetos('aguardandoProfissional')" :class="abaAtivaProjetos == 'aguardandoProfissional' ? 'ativo' : ''">
                                                Aguardando Profissional <span v-if="notificacoes.aguardando > 0" class="badge badge-pill badge-danger">{{notificacoes.aguardando}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="profissional" class="row pb-4">
                                <div class="col-12">
                                    <label class="texto-principal small" :class="abaAtivaProjetos == 'executando' || abaAtivaProjetos == 'propostas' ? '' : 'inativo'">Como Profissional</label>
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="opcao-menu-meus-projetos" @click="mudaAbaProjetos('propostas')" :class="abaAtivaProjetos == 'propostas' ? 'ativo' : ''">
                                                Propostas <span v-if="notificacoes.propostas > 0" class="badge badge-pill badge-danger">{{notificacoes.propostas}}</span>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="opcao-menu-meus-projetos" @click="mudaAbaProjetos('executando')" :class="abaAtivaProjetos == 'executando' ? 'ativo' : ''">
                                                Executando <span v-if="notificacoes.executando > 0" class="badge badge-pill badge-danger">{{notificacoes.executando}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div v-if="abaAtivaProjetos == 'todos'">
                                <table v-if="mostraProjetosTodos" class="table table-striped" >
                                    <thead class="text-center">
                                        <tr>
                                            <td><label class="texto-principal small">Nome do projeto</label></td>
                                            <td><label class="texto-principal small">Data de criação</label></td>
                                            <td><label class="texto-principal small">Visualizar</label></td>
                                            <td><label class="texto-principal small">Apagar</label></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr is="linha-projeto"
                                        v-for="projeto in projetos"
                                        v-if="projeto.fonte == 'dono'"
                                        v-bind:key="projeto.id"
                                        :projeto="projeto" :modal="mostraModal"
                                        ></tr>
                                    </tbody>
                                </table>
                                <div v-else>
                                    <div class="row">
                                        <div class="col-12">
                                            <label><h5>Parece que você não possui nenhum projeto, que tal criar um agora?<h5><label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button type="button" class="btn btn-quadrado" @click="novoProjeto()">Criar novo projeto!</button>
                                    </div>
                                </div>
                            </div> 
                            <div v-if="abaAtivaProjetos == 'emAndamento'">
                                <table v-if="mostraProjetosAndamento" class="table table-striped" >
                                    <thead class="text-center">
                                        <tr>
                                            <td><label class="texto-principal small">Nome do projeto</label></td>
                                            <td><label class="texto-principal small">Data de criação</label></td>
                                            <td><label class="texto-principal small">Visualizar</label></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr is="linha-projeto-profissional"
                                        v-for="projeto in projetos"
                                        v-if="projeto.fonte == 'dono' && projeto.aceito > 0"
                                        v-bind:key="projeto.id"
                                        :projeto="projeto" :modal="mostraModal"
                                        ></tr>
                                    </tbody>
                                </table>
                                <div v-else>
                                    <div class="row">
                                        <div class="col-12">
                                            <label><h5>Nenhum dos seus projetos está sendo executado no momento.<h5><label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="abaAtivaProjetos == 'aguardandoProfissional'">
                                <table v-if="mostraProjetosAguardando" class="table table-striped" >
                                    <thead class="text-center">
                                        <tr>
                                            <td><label class="texto-principal small">Nome do projeto</label></td>
                                            <td><label class="texto-principal small">Data de criação</label></td>
                                            <td><label class="texto-principal small">Visualizar</label></td>
                                            <td><label class="texto-principal small">Apagar</label></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr is="linha-projeto"
                                        v-for="projeto in projetos"
                                        v-if="projeto.fonte == 'dono' && projeto.aceito == 0"
                                        v-bind:key="projeto.id"
                                        :projeto="projeto" :modal="mostraModal"
                                        ></tr>
                                    </tbody>
                                </table>
                                <div v-else>
                                    <div class="row">
                                        <div class="col-12">
                                            <label><h5>Você não possui nenhum projeto com profissionais aguardando, o que acha de criar um agora?<h5><label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="button" class="btn btn-quadrado" @click="novoProjeto()">Criar novo projeto!</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="abaAtivaProjetos == 'propostas'">
                                <table v-if="mostraProjetosPropostas" class="table table-striped" >
                                    <thead class="text-center">
                                        <tr>
                                            <td><label class="texto-principal small">Nome do projeto</label></td>
                                            <td><label class="texto-principal small">Data de criação</label></td>
                                            <td><label class="texto-principal small">Visualizar</label></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr is="linha-projeto-profissional"
                                        v-for="projeto in projetos"
                                        v-if="projeto.fonte == 'chamado' && projeto.aceito == 0"
                                        v-bind:key="projeto.id"
                                        :projeto="projeto" :modal="mostraModal"
                                        ></tr>
                                    </tbody>
                                </table>
                                <div v-else>
                                    <div class="row">
                                        <div class="col-12">
                                            <label><h5>Você não possui nenhuma proposta de projeto, melhorar o seu perfil pode te ajudar a encontrar mais trabalhos!<h5><label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="button" class="btn btn-quadrado" @click="verPerfil()">Ver perfil</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="abaAtivaProjetos == 'executando'">
                                <table v-if="mostraProjetosExecutando" class="table table-striped" >
                                    <thead class="text-center">
                                        <tr>
                                            <td><label class="texto-principal small">Nome do projeto</label></td>
                                            <td><label class="texto-principal small">Data de criação</label></td>
                                            <td><label class="texto-principal small">Visualizar</label></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr is="linha-projeto-profissional"
                                        v-for="projeto in projetos"
                                        v-if="projeto.fonte == 'executando'"
                                        v-bind:key="projeto.id"
                                        :projeto="projeto"
                                        :modal="mostraModal"
                                        ></tr>
                                    </tbody>
                                </table>
                                <div v-else>
                                    <div class="row">
                                        <div class="col-12">
                                            <label><h5>Parece que você ainda não está executando nenhum projeto, que tal dar uma olhada nos projetos enviados para você?<h5><label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="button" class="btn btn-quadrado" @click="mudaAbaProjetos('propostas')">Ver projetos</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <modal-confirmacao
                        v-if="mostraModal"
                        :mensagem="mensagemModal" :titulo="tituloModal" :projeto="projetoSelecionado"
                        :botao="botaoApagar"
                    ></modal-confirmacao>
                </div>
            </div>

            <div v-else-if="abaAtiva.codigo == 'perfilProfissional'">
                <div v-if="profissional">
                    <div class="bg-light" style="margin-left: -10px; padding: 10px; margin-right: -15px;">
                        <div class="row">
                            <div class="col-6">
                                <label class="texto-principal small">Nome artístico</label>
                                <input @change="fezAlteracao()" maxlength="50" type="text" class="form-control input" id="nomeArtistico" v-model="usuario.artisticoNome" required="">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <label class="texto-principal small">Sobre você</label>
                                <textarea @keyup="fezAlteracao()" maxlength="500" rows="6" type="text" class="form-control input"  id="nomeArtistico" v-model="usuario.descricao" oninput='this.style.height = "";this.style.height = this.scrollHeight + "px"'></textarea>
                                <small class="text-muted">Caracteres: {{qtdCaracteresSobreVoce}}</small>
                            </div>
                            <!-- <div class="col-4 pr-4">
                                <i class="small">Fale um pouco sobre voce e bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</i>
                            </div> -->
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12" style="padding-left: 7px; padding-right: 10px;">
                                    <label class="texto-principal small">Cursos e Qualificações</label>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-10" style="padding-right: 0.5vw; padding-left: 7px;">
                                    <textarea maxlength="500" type="text" class="form-control input adiciona-curso textarea-curso" v-model="cursoInput" placeholder="Cursos ou qualificações..." oninput='this.style.height = "";this.style.height = this.scrollHeight + "px"'></textarea>
                                </div>
                                <div class="col-2 input form-control adiciona icone-input-container popover_wrapper" @click="adicionaCurso()">
                                    <svg class="icone-input" version="1.1" id="Capa_1" x="0px" y="0px"
                                        width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                                        xml:space="preserve">
                                        <rect x="33.214" width="3.333" height="73.799"/>
                                        <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
                                    </svg>
                                    <div class="popover-content">
                                        Adicionar Curso
                                    </div>
                                </div>
                            </div>
                            <div v-for="(curso, index) in cursos" v-bind:key="index" class="row" style="margin-bottom: 0.5vh; padding-right: 0px">
                                <div class="col-10" style="padding-right: 0.5vw; padding-left: 7px;">
                                        <textarea type="text" class="form-control input disabled textarea-curso w-100" :value="curso.descricao" disabled></textarea>
                                    </div>
                                    <div class="col-2 input apaga form-control icone-input-container popover_wrapper" @click="apagaCurso(index)">
                                        <svg class="icone-input" version="1.1" id="Capa_1" x="0px" y="0px"
                                            width="25px" height="25px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                                            xml:space="preserve">
                                            <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
                                        </svg>
                                        <div class="popover-content">
                                            Remover Curso
                                        </div>
                                </div>
                            </div>
                                
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-12" style="padding-left: 7px; padding-right: 10px;">
                                    <label class="texto-principal small">Instrumentos com os quais você trabalha</label>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12" style="padding-left: 7px; padding-right: 10px;">
                                    <seleciona-instrumento></seleciona-instrumento>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12" style="padding-left: 7px; padding-right: 10px;">
                                    <instrumentos-apaga
                                        v-for="instrumentos in instrumentos"
                                        v-bind:key="instrumentos.nome"
                                        :instrumentos="instrumentos"
                                    ></instrumentos-apaga>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <label class="texto-principal small">É especialista em quais gêneros musicais?</label>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <seleciona-especialidades></seleciona-especialidades>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12" style="padding-left: 0px; padding-right: 0px;">
                                    <especialidades-apaga
                                        v-for="especialidade in especialidades"
                                        v-bind:key="especialidade.descricao"
                                        :especialidade="especialidade"
                                    ></especialidades-apaga>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-11 offset-1" style="padding-left: 0px; padding-right: 10px;">
                                <input class="form-check-input" type="checkbox" v-model="trabalhaComVlc">
                                <label class="form-check-label">Eu trabalho com instrumentos virtuais</label>
                            </div>
                    </div>
                        </div>
                    </div>
                    <div class="row mt-4 pt-4">
                        <div class="col-12" style="padding-left: 0px; padding-right: 10px;">
                            <button type="button" class="btn btn-quadrado float-right" @click="salvaEdicaoProfissional()">Salvar alterações</button>
                            <button type="button" class="btn btn-quadrado cancelar float-right mr-2" @click="cancelaEdicao()">Cancelar</button>
                        </div>
                    </div>
                </div>  
                <div v-else>
                    <div class="row">
                        <div class="col-12">
                            <label><h5>Você não está cadastrado como um profissional</h5></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn btn-quadrado" @click="viraProfissional()">Quero ser um profissional!</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div v-else-if="abaAtiva.codigo == 'editaPortfolio'">
                <div class="row">
                    <div class="col-12">
                        <p>Aqui você pode adicionar os projetos que já realizou para que outras 
                        pessoas possam conhecer melhor o seu trabalho</p>
                    </div>
                </div>
                <div class="row">
                    <portfolio
                        v-for="(projeto, index) in portfolio"
                        v-bind:key="index"
                        :projeto="projeto"
                    ></portfolio>
                    <div class="col-4 d-flex mb-3 novoProjetoPortifolio" style="padding-left:0px;">
                        <div class="input form-control adiciona icone-input-container" @click="novoProjetoPortfolio()" style="min-height: 200px;">
                            <svg class="icone-input" version="1.1" id="Capa_1" x="0px" y="0px"
                                width="30px" height="30px" viewBox="-1.916 0 73.798 73.799" enable-background="new -1.916 0 73.798 73.799"
                                xml:space="preserve">
                                <rect x="33.214" width="3.333" height="73.799"/>
                                <rect x="-1.916" y="35.232" width="73.798" height="3.334"/>
                            </svg>          
                            <label class="label-novo-projeto">     
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="modal fade" id="modalProjetoPortfolio" role="dialog" data-backdrop="static">
            <div class="modal-dialog w-75" style="max-width: 100%;">                              
                <div class="modal-content quina-viva">
                    <div class="modal-header">
                        <h4 class="modal-title texto-principal grande">{{projetoPortfolioSelecionado.nome}}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-8">
                                <div class="row">
                                    <div class="col-12">
                                        <label class="texto-principal small">Nome do projeto</label>
                                        <input maxlength="100" type="text" class="form-control input" v-model="projetoPortfolioSelecionado.nome">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <label class="texto-principal small">Descrição</label>
                                        <textarea maxlength="500" rows="4" type="text" class="form-control input" v-model="projetoPortfolioSelecionado.descricao" oninput='this.style.height = "";this.style.height = this.scrollHeight + "px"'></textarea>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-12">
                                                <label class="texto-principal medio">Link externo</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <label class="texto-principal small">Site externo</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="youtube" v-model="projetoPortfolioSelecionado.tipo">
                                                    <label class="form-check-label" for="youtube">
                                                        YouTube
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" value="spotify" v-model="projetoPortfolioSelecionado.tipo">
                                                    <label class="form-check-label" for="spotify">
                                                        Spotify
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-8">
                                                <label class="texto-principal small">Link</label>
                                                <input maxlength="100000" type="text" class="form-control input" v-model="projetoPortfolioSelecionado.link">
                                                <small class="text-muted">{{orientacoesLinkNovoProjeto()}}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <projeto-portfolio
                                        :projeto="projetoPortfolioSelecionado"
                                    ></projeto-portfolio>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-12" align="center">
                                        <small class="text-muted">
                                            Essa é a forma como esse projeto aparecerá no seu portfólio
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-quadrado cancelar w-100" @click="deletaProjeto()">Apagar projeto</button>
                        <button type="button" class="btn btn-quadrado cancelar w-100" class="close" data-dismiss="modal">Cancelar alterações</button>
                        <button type="button" class="btn btn-quadrado w-100" @click="atualizaProjeto()">Salvar alterações</button> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
    include "../rodape/rodape.php"
?>

<script src="../js/popper.min.js" crossorigin="anonymous"></script>
<script src="meuPerfil.js"></script>
<script src="../bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
</BODY>
</HTML>