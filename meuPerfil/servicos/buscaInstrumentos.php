<?php
include "../../sessao.php";

$_GET['term'] = "%".$_GET['term']."%";
$_GET['term'] = trim(strip_tags(utf8_decode($_GET['term'])));

$query = "SELECT i.* FROM `instrumentos` i 
            WHERE lower(i.nome) LIKE lower((?))
            AND i.id NOT IN (
                SELECT pi.id_instrumentos FROM profissional p
                INNER JOIN profissional_instrumentos pi ON pi.id_profissional = p.id
                WHERE p.id_usuario = (?)
            )
            ORDER BY i.NOME";
$query = $conexao->prepare($query);

$query->bind_param("si", $_GET['term'], $idLogado);

$query->execute();
$res = $query->get_result();
$nomes = array();

$i = 0;
while ($n = $res -> fetch_assoc()) {
    $nomes[$i]["id"] = utf8_encode($n['id']);
    $nomes[$i]["label"] = utf8_encode($n['nome']);
    $i = $i + 1;
}
$conexao->close();
echo json_encode($nomes);
?>