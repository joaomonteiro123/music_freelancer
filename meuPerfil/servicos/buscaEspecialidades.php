<?php
include "../../sessao.php";

$_GET['term'] = "%".$_GET['term']."%";
$_GET['term'] = trim(strip_tags(utf8_decode($_GET['term'])));

$query = "SELECT * FROM `especialidades` WHERE lower(descricao) LIKE
lower((?)) ORDER BY descricao";

$query = "SELECT e.* FROM `especialidades` e 
            WHERE lower(e.descricao) LIKE lower((?))
            AND e.id NOT IN (
                SELECT pe.id_especialidades FROM profissional p
                INNER JOIN profissional_especialidades pe ON pe.id_profissional = p.id
                WHERE p.id_usuario = (?)
            )
            ORDER BY e.descricao";

$query = $conexao->prepare($query);
$query->bind_param("si", $_GET['term'], $idLogado);

$query->execute();
$res = $query->get_result();
$nomes = array();

$i = 0;
while ($n = $res -> fetch_assoc()) {
    $nomes[$i]["id"] = utf8_encode($n['id']);
    $nomes[$i]["label"] = utf8_encode($n['descricao']);
    $i = $i + 1;
}
$conexao->close();
echo json_encode($nomes);
?>