<?php
//upload.php
include "../../sessao.php";

$idUsuario = $_SESSION['idUsuario'];

if(count($_FILES["file"]["name"]) > 0){
    sleep(3);
    $tmp_name = $_FILES["file"]['tmp_name'][0];
    
    $query = "UPDATE usuario SET foto = (?) WHERE id = (?)";
    $null = NULL;
    $query = $conexao->prepare($query);
    $query->bind_param("bi", $null, $idUsuario);
    $query ->send_long_data(0, file_get_contents($tmp_name));
    $query->execute();
}

$conexao->close();
?>