<?php
include "../../sessao.php";
$id = $_SESSION['idUsuario'];

$query = "SELECT p.id, p.nome, p.data_criacao, 'dono' as fonte, SUM(pe.fl_aceito) as aceito, COUNT(pe.id) as envio, pe.fl_notificacao_dono as notificacao
        FROM projeto p
        LEFT JOIN projeto_envio pe ON pe.id_projeto = p.id
        INNER JOIN projeto_instrumentista pi ON p.id = pi.id_projeto
        WHERE p.id_usuario = (?) AND p.fl_ativo = 1
        GROUP BY p.id, pe.fl_notificacao_dono

        UNION

        SELECT p.id, p.nome, p.data_criacao, IF(pe.fl_aceito = 1,'executando','chamado') as fonte, aceito.aceitos as aceito, null as envio, pe.fl_notificacao_profissional as notificacao
        FROM projeto_envio pe
        INNER JOIN (SELECT SUM(pe2.fl_aceito) as aceitos, pe2.id_projeto
                    from projeto_envio pe2 GROUP BY id_projeto) aceito
        INNER JOIN profissional pro ON pro.id = pe.id_profissional
        INNER JOIN projeto p ON p.id = pe.id_projeto
        WHERE pro.id_usuario = (?)
            AND p.id = aceito.id_projeto
            AND pe.fl_aceito>= (SELECT SUM(pe2.fl_aceito)
            FROM projeto_envio pe2 WHERE pe2.id_projeto = pe.id_projeto) 
        GROUP BY p.id, pe.fl_aceito, pe.fl_notificacao_profissional
        ORDER BY data_criacao desc";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('ii',$id, $id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}

$res = $query->get_result();

$i = 0;
$projeto = array();
while ($n = $res -> fetch_assoc()) {
    $projeto[$i]['nome'] = utf8_encode($n['nome']);
    $n['data_criacao'] = date("d-m-Y", strtotime($n['data_criacao']));
    $projeto[$i]['data'] = $n['data_criacao'];
    $projeto[$i]['id'] = $n['id'];
    $projeto[$i]['fonte'] = $n['fonte'];
    $projeto[$i]['aceito'] = $n['aceito'];
    $projeto[$i]['envio'] = $n['envio'];
    $projeto[$i]['notificacao'] = $n['notificacao'];
    $i = $i + 1;
}

$conexao->close();
echo json_encode($projeto);
?>





