<?php
//upload.php
include "../../sessao.php";

$idProfissional = $_POST['idProfissional'];
$idInstrumentos = $_POST['instrumentos'];
$idInstrumentos = explode(',', $idInstrumentos);

foreach ($idInstrumentos as $instrumento){
    $query = "DELETE FROM profissional_instrumentos WHERE id_profissional = (?) AND id_instrumentos = (?)";
    $query = $conexao->prepare($query);
    $query->bind_param("ii", $idProfissional, $instrumento);
    !$query->execute();
}
$conexao->close();
exit;
?>