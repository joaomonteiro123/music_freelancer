<?php
//upload.php
include "../../sessao.php";

$idUsuario = $_SESSION['idUsuario'];
$email = utf8_decode($_POST['email']);
$telefoneFixo = $_POST['telefone_fixo'];
$telefoneCelular = $_POST['telefone_celular'];
$rg = $_POST['rg'];
$cpf = $_POST['cpf'];
$profissao = utf8_decode($_POST['profissao']);


$query = "UPDATE usuario SET telefone_fixo = (?), telefone_celular = (?),
rg = (?), cpf = (?), profissao = (?) WHERE id = (?)";
$query = $conexao->prepare($query);
$query->bind_param("sssssi", $telefoneFixo, $telefoneCelular, $rg, $cpf, $profissao, $idUsuario);
if (!$query->execute() === true){
    $conexao->close();
    echo "erro";
    exit;
}
$conexao->close();
?>