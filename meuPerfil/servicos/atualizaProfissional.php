<?php
//upload.php
include "../../sessao.php";

$idUsuario = $_SESSION['idUsuario'];
$nomeArtistico = ($_POST['artisticoNome']);
$descricao = ($_POST['descricao']);

$query = "UPDATE profissional SET nome_artistico = (?), descricao = (?), vlc = (?) WHERE id_usuario = (?) ";
$query = $conexao->prepare($query);
$query->bind_param("ssii", utf8_decode($nomeArtistico), utf8_decode($descricao), $_POST['vlc'], $idUsuario);
if (!$query->execute() === true){
    $conexao->close();
    echo "erro";
    exit;
}

$instrumentos = $_POST['instrumentos'];
$idProfissional = $_POST['id'];

foreach ($instrumentos as $instrumento){
    $query = "SELECT * FROM `profissional_instrumentos`
            WHERE id_profissional = (?) AND id_instrumentos = (?)";
    $query = $conexao->prepare($query);
    $query->bind_param("ii", $idProfissional, $instrumento['id']);
    $query->execute();
    $query = $query -> get_result();
    if (!$query->num_rows > 0){
        $query = "INSERT INTO `profissional_instrumentos` (`id`, `id_profissional`, `id_instrumentos`)
            VALUES (NULL, (?), (?))";
        $query = $conexao->prepare($query);
        $query->bind_param("ii", $idProfissional, $instrumento['id']);
        if (!$query->execute() === true){
            $conexao->close();
            echo "erro";
            exit;
        }
    }
}

$especialidades = $_POST['especialidades'];

foreach ($especialidades as $especialidade){
    $query = "SELECT * FROM `profissional_especialidades`
            WHERE id_profissional = (?) AND id_especialidades = (?)";
    $query = $conexao->prepare($query);
    $query->bind_param("ii", $idProfissional, $especialidade['id']);
    $query->execute();
    $query = $query -> get_result();
    if (!$query->num_rows > 0){
        $query = "INSERT INTO `profissional_especialidades` (`id`, `id_profissional`, `id_especialidades`)
            VALUES (NULL, (?), (?))";
        $query = $conexao->prepare($query);
        $query->bind_param("ii", $idProfissional, $especialidade['id']);
        if (!$query->execute() === true){
            $conexao->close();
            echo "erro";
            exit;
        }
    }
}

$cursos = $_POST['cursos'];

foreach ($cursos as $curso){
    $query = "INSERT INTO `curso_profissional` (`id`, `id_profissional`, `descricao`)
        VALUES (NULL, (?), (?))";
    $query = $conexao->prepare($query);
    $query->bind_param("is", $idProfissional, utf8_decode($curso['descricao']));
    if (!$query->execute() === true){
        $conexao->close();
        echo "erro";
        exit;
    }
}

$conexao->close();
?>