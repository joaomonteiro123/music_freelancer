<?php
include "../../sessao.php";
$id = $_SESSION['idUsuario'];

$usuario = array();

$query = "SELECT u.primeiro_nome, u.ultimo_nome, u.foto, u.email, p.id, p.nome_artistico,
p.descricao, u.telefone_celular, u.telefone_fixo, u.endereco, u.rg, u.cpf, u.profissao, p.vlc
FROM usuario u LEFT JOIN profissional p ON p.id_usuario = u.id
WHERE u.id = (?)";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query->bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}

$res = $query->get_result();
$n = $res -> fetch_assoc();
$usuario['primeiroNome'] = utf8_encode($n['primeiro_nome']);
$usuario['ultimoNome'] = utf8_encode($n['ultimo_nome']);
$usuario['artisticoNome'] = utf8_encode($n['nome_artistico']);
$usuario['descricao'] = utf8_encode($n['descricao']);
$usuario['email'] = utf8_encode($n['email']);
$usuario['id'] = $n['id'];
$usuario['telefone_fixo'] = $n['telefone_fixo'];
$usuario['telefone_celular'] = $n['telefone_celular'];
$usuario['rg'] = $n['rg'];
$usuario['cpf'] = $n['cpf'];
$usuario['profissao'] = utf8_encode($n['profissao']);
$usuario['endereco'] = utf8_encode($n['endereco']);
$usuario['vlc'] = utf8_encode($n['vlc']);

if ( $n['foto'] == "" ){
    $n['foto'] = "../images/generic_user.png";
}else{
    $n['foto'] = "../servicos/imagemUsuario.php?id=".$id;
}   
$usuario['foto'] = $n['foto'];


$query = "SELECT DISTINCT i.id, i.nome FROM profissional_instrumentos pi INNER JOIN instrumentos i
ON i.id = pi.id_instrumentos INNER JOIN profissional p ON p.id = pi.id_profissional
WHERE p.id_usuario = (?)";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}
$res = $query->get_result();
$instrumentos = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $instrumentos[$i]['nome'] = utf8_encode($n['nome']);
    $instrumentos[$i]['id'] = $n['id']; 
    $i = $i + 1;
}
$usuario['instrumentos'] = $instrumentos;


$query = "SELECT DISTINCT e.id, e.descricao FROM profissional_especialidades pe INNER JOIN especialidades e
ON e.id = pe.id_especialidades INNER JOIN profissional p ON p.id = pe.id_profissional
WHERE p.id_usuario = (?)";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}
$res = $query->get_result();
$especialidades = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $especialidades[$i]['descricao'] = utf8_encode($n['descricao']);
    $especialidades[$i]['id'] = $n['id']; 
    $i = $i + 1;
}
$usuario['especialidades'] = $especialidades;


$query = "SELECT DISTINCT cp.id, cp.descricao FROM curso_profissional cp
INNER JOIN profissional p ON p.id = cp.id_profissional
WHERE p.id_usuario = (?)";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}
$res = $query->get_result();
$cursos = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $cursos[$i]['descricao'] = utf8_encode($n['descricao']);
    $cursos[$i]['id'] = $n['id']; 
    $i = $i + 1;
}
$usuario['cursos'] = $cursos;


$query = "SELECT DISTINCT pp.id, pp.link, pp.tipo, pp.nome, pp.descricao FROM portfolioprofissional pp
INNER JOIN profissional p ON p.id = pp.id_profissional
WHERE p.id_usuario = (?) ORDER BY pp.id ASC";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}
$res = $query->get_result();
$portfolio = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $portfolio[$i]['link'] = utf8_encode($n['link']);
    $portfolio[$i]['tipo'] = utf8_encode($n['tipo']);
    $portfolio[$i]['nome'] = utf8_encode($n['nome']);
    $portfolio[$i]['descricao'] = utf8_encode($n['descricao']);
    $portfolio[$i]['id'] = $n['id']; 
    $i = $i + 1;
}
$usuario['portfolio'] = $portfolio;


$query = "SELECT COUNT(DISTINCT pe.id) as projetos FROM projeto_envio pe
INNER JOIN profissional p ON p.id = pe.id_profissional
WHERE p.id_usuario = (?)
AND pe.fl_aceito>=
    (SELECT SUM(pe2.fl_aceito)
    FROM projeto_envio pe2
    WHERE pe2.id_projeto = pe.id_projeto)";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}
$res = $query->get_result();
$instrumentos = array();
$n = $res -> fetch_assoc();
$usuario['projetosContratados'] = $n['projetos'];


$query = "SELECT COUNT(DISTINCT pe.id) as projetos FROM projeto pe
WHERE pe.id_usuario = (?) AND pe.fl_ativo  =1";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}
$res = $query->get_result();
$instrumentos = array();
$n = $res -> fetch_assoc();
$usuario['projetosSolicitados'] = $n['projetos'];


$conexao->close();
echo json_encode($usuario);
?>