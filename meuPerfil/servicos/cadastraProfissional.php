<?php
//upload.php
include "../../sessao.php";

$idUsuario = $_SESSION['idUsuario'];

$query = "SELECT * FROM profissional WHERE id_usuario = (?)";
$query = $conexao->prepare($query);
$query->bind_param("i", $idUsuario);
$query->execute();
$query = $query -> get_result();
if ($query->num_rows > 0){
    $conexao->close();
    header('Location: ../meuPerfil.php'); 
}else{
    $usuarioLogado = UTF8_DECODE($usuarioLogado);
    $query = "INSERT INTO `profissional` (`id`, `id_usuario`, `nome_artistico`, `descricao`)
    VALUES (NULL, (?), (?), '')";
    $query = $conexao->prepare($query);
    $query->bind_param("is", $idUsuario, $usuarioLogado);
    $query->execute();
}
$conexao->close();
?>