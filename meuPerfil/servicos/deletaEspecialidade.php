<?php
//upload.php
include "../../sessao.php";

$idProfissional = $_POST['idProfissional'];
$idEspecialidades = $_POST['especialidades'];
$idEspecialidades = explode(',', $idEspecialidades);

foreach ($idEspecialidades as $especialidade){
    $query = "DELETE FROM profissional_especialidades WHERE id_profissional = (?) AND id_especialidades = (?)";
    $query = $conexao->prepare($query);
    $query->bind_param("ii", $idProfissional, $especialidade);
    !$query->execute();
}
$conexao->close();
exit;
?>