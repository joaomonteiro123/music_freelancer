Vue.component("linha-projeto", {
    props: ["projeto", "mostraModal"],
    template: '#tabela-template',
    methods: {
        abreProjeto: function (id) {
            window.location = '../abreProjeto/abreProjeto.php?id=' + id;
        },
        apagaProjeto: function (id, aceito) {
            if (aceito > 0) {
                swal("Erro", "Você não pode apagar um projeto já contratado", "error");
            } else {
                meusProjetos.projetoSelecionado = id;
                meusProjetos.mostraModal = true;
            }
        }
    },
})
Vue.component("linha-projeto-profissional", {
    props: ["projeto"],
    template: '#tabela-profissional-template',
    methods: {
        abreProjeto: function (id) {
            window.location = '../abreProjeto/abreProjeto.php?id=' + id;
        },
    },
})

Vue.component("modal-confirmacao", {
    props: ["mensagem", "titulo", "projeto", "botao"],
    template: '#modal-apaga',
    methods: {
        cancela: function () {
            meusProjetos.projetoSelecionado = null;
            meusProjetos.mostraModal = false;
        },
        confirma: function () {
            meusProjetos.botaoApagar = "Aguarde..."
            $.ajax({
                url: 'servicos/apagaProjeto.php',
                data: "id=" + this.projeto,
                type: "POST",
                success: function (e) {
                    meusProjetos.mostraModal = false;
                    meusProjetos.$http.get("servicos/carregaProjetos.php").then(function (resposta) {
                        this.projetos = resposta.data;
                        this.projetos.length > 0 ? this.mostraProjetosTodos = true : this.mostraProjetosTodos = false
                    });
                    meusProjetos.botaoApagar = "Confirmar"
                }
            });
        },
    },
})

Vue.component("portfolio", {
    props: ["projeto"],
    template: '#portfolio-template',
    methods: {
        abreProjeto: function (projeto) {
            meuPerfil.projetoPortfolioSelecionado = projeto;
        },
        linkYouTube: function (link) {
            n = link.lastIndexOf('/');
            m = link.substring(n + 1).lastIndexOf('v=');
            if (m == -1)
                return "https://www.youtube.com/embed/" + link.substring(n + 1)
            else {
                return "https://www.youtube.com/embed/" + link.substring(n + m + 3)
            }
        },
        linkSpotify(link) {
            let newLink = link.slice(0, 25) + 'embed/' + link.slice(25);
            width = $('.novoProjetoPortifolio').width();
            return `<iframe src='${newLink}' class='card-projetoSpotify'
                        width="${width}" height="${width * 1.26}" frameborder="0" allowtransparency="true" allow="encrypted-media">
                    </iframe>`
        }
    },
})

Vue.component("projeto-portfolio", {
    props: ["projeto"],
    template: '#projeto-portfolio-template',
    methods: {
        linkYouTube: function (link) {
            n = link.lastIndexOf('/');
            m = link.substring(n + 1).lastIndexOf('v=');
            if (m == -1)
                return "https://www.youtube.com/embed/" + link.substring(n + 1)
            else {
                return "https://www.youtube.com/embed/" + link.substring(n + m + 3)
            }
        },
        linkSpotify(link) {
            let newLink = link.slice(0, 25) + 'embed/' + link.slice(25);
            return `<iframe src='${newLink}'
                        width="200" height="254" frameborder="0" allowtransparency="true" allow="encrypted-media">
                    </iframe>`
        }
    },
})

Vue.component("instrumentos", {
    props: ["instrumentos"],
    template: '#instrumentos-template',
})

Vue.component("especialidades", {
    props: ["especialidade"],
    template: '#especialidades-template',
})

Vue.component("instrumentos-apaga", {
    props: ["instrumentos"],
    template: '#instrumentos-apaga-template',
    methods: {
        apagaInstrumento: function (idInstrumento) {
            meuPerfil.instrumentosApagados.push(idInstrumento);
            let i = meuPerfil.usuario.instrumentos.map(item => item.id).indexOf(idInstrumento);
            meuPerfil.usuario.instrumentos.splice(i, 1);
        },
    },
})

Vue.component("especialidades-apaga", {
    props: ["especialidade"],
    template: '#especialidades-apaga-template',
    methods: {
        apagaEspecialidade: function (idEspecialidade) {
            meuPerfil.especialidadesApagadas.push(idEspecialidade);
            let i = meuPerfil.usuario.especialidades.map(item => item.id).indexOf(idEspecialidade);
            meuPerfil.usuario.especialidades.splice(i, 1);
        },
    },
})

Vue.component("seleciona-instrumento", {
    template: '#seleciona-intrumento-temaplate',
    mounted: function () {
        $("#inputInstrumento").autocomplete({
            source: "servicos/buscaInstrumentos.php",
            minLength: 0,
            select: function (event, ui) {
                if (ui.item == null) {
                    swal("Atenção", "É necessário selecionar um instrumento válido", "warning");
                    $("#inputInstrumento").val("");
                    $("#inputInstrumento").focus();
                } else {
                    existe = false;
                    for (var i = 0; i < meuPerfil.usuario.instrumentos.length; i++) {
                        if (meuPerfil.usuario.instrumentos[i].id == ui.item['id']) {
                            existe = true;
                            break;
                        }
                    }
                    if (!existe) {
                        meuPerfil.usuario.instrumentos.push({ "id": ui.item['id'], "nome": ui.item['label'] });
                        meuPerfil.fezAlteracao();
                    }

                    $("#inputInstrumento").val("");
                    $("#inputInstrumento").focus();
                }
            },
            change: function (event, ui) {
                $("#inputInstrumento").val("");
                $("#inputInstrumento").focus();
            }
        });
    },
    methods: {
        mostraInstrumentos: function () {
            $("#inputInstrumento").autocomplete("search", "");
        },
    },
})

Vue.component("seleciona-especialidades", {
    template: '#seleciona-especialidades-temaplate',
    mounted: function () {
        $("#inputEspecialidade").autocomplete({
            source: "servicos/buscaEspecialidades.php",
            minLength: 0,
            select: function (event, ui) {
                if (ui.item == null) {
                    swal("Atenção", "É necessário selecionar uma especialidade válida", "warning");
                    $("#inputEspecialidade").val("");
                    $("#inputEspecialidade").focus();
                } else {
                    existe = false;
                    for (var i = 0; i < meuPerfil.usuario.especialidades.length; i++) {
                        if (meuPerfil.usuario.especialidades[i].id == ui.item['id']) {
                            existe = true;
                            break;
                        }
                    }
                    if (!existe) {
                        meuPerfil.usuario.especialidades.push({ "id": ui.item['id'], "descricao": ui.item['label'] });
                        meuPerfil.fezAlteracao();
                    }

                    $("#inputEspecialidade").val("");
                    $("#inputEspecialidade").focus();
                }
            },
            change: function (event, ui) {
                $("#inputEspecialidade").val("");
                $("#inputEspecialidade").focus();
            }
        });
    },
    ready: () => {
        window.addEventListener('resize', this.handleResize)
    },
    beforeDestroy: () => {
        window.removeEventListener('resize', this.handleResize)
    },
    methods: {
        mostraEspecialidades: function () {
            $("#inputEspecialidade").autocomplete("search", "");
        },
    },
})


meusProjetos = meuPerfil = new Vue({
    el: "#app",
    data: {
        usuario: [],
        profissional: false,
        uploadFotoMessage: "",
        instrumentos: [],
        instrumentosApagados: [],
        especialidades: [],
        especialidadesApagadas: [],
        trabalhaComVlc: false,
        cursos: [],
        cursosApagados: [],
        cursoInput: "",
        portfolio: [],
        projetoPortfolioSelecionado: [],
        modoEdicaoProfissional: false,
        abaAtiva: {
            codigo: 'projetos',
            nome: "Meus Projetos"
        },

        projetos: [],
        mostraProjetosTodos: false,
        mostraProjetosAndamento: false,
        mostraProjetosAguardando: false,
        mostraProjetosPropostas: false,
        mostraProjetosExecutando: false,
        mostraModal: false,
        mensagemModal: "Você tem certeza que deseja excluir o projeto? Essa ação não pode ser desfeita",
        tituloModal: "Apagar projeto",
        botaoApagar: "Confirmar",
        projetoSelecionado: null,
        abaAtivaProjetos: 'todos',

        qtdCaracteresSobreVoce: 0,
        notificacoes: {
            todos: 0,
            andamento: 0,
            aguardando: 0,
            propostas: 0,
            executando: 0
        },
    },
    created: async function () {
        $("body").addClass("loading");
        await this.$http.get("servicos/buscaUsuario.php").then(function (resposta) {
            this.usuario = resposta.data;
            this.usuario.id == null ? this.profissional = false : this.profissional = true
            this.instrumentos = this.usuario.instrumentos;
            this.especialidades = this.usuario.especialidades;
            this.portfolio = this.usuario.portfolio;
            this.cursos = this.usuario.cursos;
            this.trabalhaComVlc = parseInt(this.usuario.vlc) ? true : false;
        });

        await this.$http.get("servicos/carregaProjetos.php").then(function (resposta) {
            this.projetos = resposta.data;

            this.projetos.forEach(el => {
                if (el.notificacao) console.log(el)
            })

            this.projetos.length > 0 ? this.mostraProjetosTodos = true : this.mostraProjetosTodos = false;
            for (var i = 0; i < this.projetos.length; i++) {
                if (this.projetos[i].fonte == 'dono') {
                    if (this.projetos[i].notificacao) {
                        this.notificacoes.todos += 1;
                    }
                }

                if (this.projetos[i].aceito > 0 && this.projetos[i].fonte == 'dono') {
                    this.mostraProjetosAndamento = true;
                    if (this.projetos[i].notificacao) {
                        this.notificacoes.andamento += 1;
                    }
                }

                if (this.projetos[i].aceito == 0 && this.projetos[i].fonte == 'dono' && this.projetos[i].envio > 0) {
                    this.mostraProjetosAguardando = true;
                    if (this.projetos[i].notificacao) {
                        this.notificacoes.aguardando += 1;
                    }
                }

                if (this.projetos[i].aceito == 0 && this.projetos[i].fonte == 'chamado') {
                    this.mostraProjetosPropostas = true;
                    if (this.projetos[i].notificacao) {
                        this.notificacoes.propostas += 1;
                    }
                }

                if (this.projetos[i].fonte == 'executando') {
                    this.mostraProjetosExecutando = true;
                    if (this.projetos[i].notificacao) {
                        this.notificacoes.executando += 1;
                    }
                }

                if (this.mostraProjetosAndamento == true && this.mostraProjetosExecutando && this.mostraProjetosPropostas && this.mostraProjetosExecutando) {
                    break;
                }
            }
        });

        let url_string = window.location.href;
        let url = new URL(url_string);
        let profissional = url.searchParams.get("profissional");
        if (profissional) {
            this.mudaAba('perfilProfissional');
        }

        $("body").removeClass("loading");
    },
    methods: {
        novoProjeto: function () {
            window.location = '../novoProjeto/novoProjeto.php';
        },

        mudaAbaProjetos: function (aba) {
            this.abaAtivaProjetos = aba;
        },

        verPerfil: function () {
            this.abaAtiva.codigo = "perfilProfissional";
            this.abaAtiva.nome = "Perfil profissional";
            Vue.nextTick().then(function () {
                $(".textarea-curso").each((index, element) => {
                    element.style.height = "";
                    element.style.height = element.scrollHeight + "px";
                });
            })
        },

        clickInputFoto: function () {
            $("#input-foto").trigger("click");
        },

        apagaCurso: function (index) {
            if (this.cursos[index].id != 0) this.cursosApagados.push(this.cursos[index].id);
            this.usuario.cursos.splice(index, 1);
        },

        adicionaCurso: function () {
            this.fezAlteracao();
            if (this.cursoInput.length > 0) {
                this.usuario.cursos.push({
                    descricao: this.cursoInput,
                    id: 0
                });
                this.cursoInput = "";
            }
        },

        deletaProjeto: function () {
            swal({
                title: "Você tem certeza?",
                text: "Essa ação não poderá ser desfeita!",
                icon: "error",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    id = this.projetoPortfolioSelecionado.id;
                    $.ajax({
                        url: "servicos/apagaProjetoPortfolio.php",
                        method: "POST",
                        data: "id=" + id,
                        success: function (resposta) {
                            if (resposta == "erro") {
                                swal("Erro", "Um erro inesperado ocorreu", "error");
                            } else {
                                swal("Pronto!", "Item do portifólio apagado com sucesso!", "success");
                                meuPerfil.$http.get("servicos/buscaUsuario.php").then(function (resposta) {
                                    meuPerfil.usuario = resposta.data;
                                    meuPerfil.portfolio = meuPerfil.usuario.portfolio;
                                });
                                $("#modalProjetoPortfolio").modal('hide')
                            }
                        }
                    });
                }
            });
        },
        atualizaProjeto: function () {
            dataProjeto = this.projetoPortfolioSelecionado;
            $.ajax({
                url: "servicos/atualizaProjeto.php",
                method: "POST",
                data: dataProjeto,
                success: function (resposta) {
                    if (resposta == "erro") {
                        swal("Erro", "Um erro inesperado ocorreu", "error");
                    } else {
                        swal("Pronto!", "Item do portifólio atualizado com sucesso!", "success");
                        meuPerfil.$http.get("servicos/buscaUsuario.php").then(function (resposta) {
                            meuPerfil.usuario = resposta.data;
                            meuPerfil.portfolio = meuPerfil.usuario.portfolio;
                        });
                        Vue.nextTick().then(function () {
                            numeroProjetos = meuPerfil.portfolio.length
                            meuPerfil.projetoPortfolioSelecionado = meuPerfil.portfolio[numeroProjetos - 1];
                            $('#modalProjetoPortfolio').modal('hide');
                        })
                    }
                }
            });
        },
        novoProjetoPortfolio: function () {
            numeroProjetos = meuPerfil.portfolio.length
            $.ajax({
                url: "servicos/novoProjeto.php",
                method: "POST",
                data: "idProfissional=" + meuPerfil.usuario.id,
                success: function (resposta) {
                    if (resposta == "erro") {
                        swal("Erro", "Um erro inesperado ocorreu", "error");
                    } else {
                        meuPerfil.$http.get("servicos/buscaUsuario.php").then(function (resposta) {
                            meuPerfil.usuario = resposta.data;
                            meuPerfil.portfolio = meuPerfil.usuario.portfolio;
                            meuPerfil.$nextTick().then(function () {
                                meuPerfil.projetoPortfolioSelecionado = meuPerfil.portfolio[numeroProjetos];
                                $('#modalProjetoPortfolio').modal('show');
                            })
                        });
                    }
                }
            });
        },
        mudaAba: function (aba) {
            this.abaAtiva.codigo = aba;
            switch (aba) {
                case "projetos":
                    this.abaAtiva.nome = "Meus Projetos";
                    break;

                case "editaConta":
                    this.abaAtiva.nome = "Detalhes da Conta";
                    break;

                case "editaPortfolio":
                    this.abaAtiva.nome = "Meu portfólio";
                    break;

                case "perfilProfissional":
                    this.abaAtiva.nome = "Perfil profissional";
                    Vue.nextTick().then(function () {
                        $(".textarea-curso").each((index, element) => {
                            element.style.height = "";
                            element.style.height = element.scrollHeight + "px";
                        });
                    });
                    break;
            }
        },

        meusProjetos: function () {
            this.abaAtiva = {
                codigo: 'projetos',
                nome: "Meus Projetos"
            };
        },

        edicaoProfissional: function () {
            this.modoEdicaoProfissional == false ? this.modoEdicaoProfissional = true : this.modoEdicaoProfissional = false;
        },
        cancelaEdicao: function () {
            this.modoEdicaoProfissional = false;
            this.atualizaDados();
        },
        salvaEdicaoProfissional: async function () {
            var contador = 0;

            var data = meuPerfil.instrumentosApagados;
            await $.ajax({
                url: "servicos/deletaInstrumento.php",
                method: "POST",
                data: "instrumentos=" + data + "&idProfissional=" + meuPerfil.usuario.id,
                success: function (resposta) {
                    if (resposta == "erro") {
                        swal("Erro", "Um erro inesperado ocorreu", "error");
                    } else {
                        meuPerfil.instrumentosApagados = [];
                        contador++;
                    }
                }
            });
            var data = meuPerfil.especialidadesApagadas;
            await $.ajax({
                url: "servicos/deletaEspecialidade.php",
                method: "POST",
                data: "especialidades=" + data + "&idProfissional=" + meuPerfil.usuario.id,
                success: function (resposta) {
                    if (resposta == "erro") {
                        swal("Erro", "Um erro inesperado ocorreu", "error");
                    } else {
                        meuPerfil.especialidadesApagadas = [];
                        contador++;
                    }
                }
            });
            var data = meuPerfil.cursosApagados;
            await $.ajax({
                url: "servicos/deletaCurso.php",
                method: "POST",
                data: "cursos=" + data + "&idProfissional=" + meuPerfil.usuario.id,
                success: function (resposta) {
                    if (resposta == "erro") {
                        swal("Erro", "Um erro inesperado ocorreu", "error");
                    } else {
                        meuPerfil.cursosApagados = [];
                        contador++;
                    }
                }
            });
            dataUsuario = meuPerfil.usuario;
            dataUsuario.cursos = meuPerfil.usuario.cursos.filter(curso => {
                return curso.id == 0
            });

            if (this.cursoInput) {
                dataUsuario.cursos.push({
                    id: 0,
                    descricao: this.cursoInput
                });
            }

            dataUsuario.vlc = this.trabalhaComVlc ? 1 : 0;

            await $.ajax({
                url: "servicos/atualizaProfissional.php",
                method: "POST",
                data: dataUsuario,
                success: function (resposta) {
                    if (resposta == "erro") {
                        swal("Erro", "Um erro inesperado ocorreu", "error");
                    } else {
                        contador++;
                    }
                }
            });

            if (contador == 4) {
                swal("Pronto!", "Os seus dados foram atualizados", "success");
                window.onbeforeunload = null;
            }
            else swal("Erro", "Um erro inesperado ocorreu", "error");

            $("body").addClass("loading");
            this.$http.get("servicos/buscaUsuario.php").then(function (resposta) {
                this.usuario = resposta.data;
                this.usuario.id == null ? this.profissional = false : this.profissional = true
                this.instrumentos = this.usuario.instrumentos;
                this.especialidades = this.usuario.especialidades;
                this.portfolio = this.usuario.portfolio;
                this.cursos = this.usuario.cursos;
                this.cursoInput = "";
                Vue.nextTick().then(function () {
                    $(".textarea-curso").each((index, element) => {
                        element.style.height = "";
                        element.style.height = element.scrollHeight + "px";
                    });
                })
                $("body").removeClass("loading");
            });
        },
        salvaEdicaoBasico: function () {
            dataUsuario = meuPerfil.usuario;
            $.ajax({
                url: "servicos/atualizaBasico.php",
                method: "POST",
                data: dataUsuario,
                success: function (resposta) {
                    if (resposta == "erro") {
                        swal("Erro", "Um erro inesperado ocorreu", "error");
                    } else {
                        swal("Pronto!", "Os seus dados foram atualizados", "success");
                    }
                }
            });
        },
        atualizaDados: function () {
            $("body").addClass("loading");
            this.$http.get("servicos/buscaUsuario.php").then(function (resposta) {
                this.usuario = resposta.data;
                this.usuario.id == null ? this.profissional = false : this.profissional = true;
                this.instrumentos = this.usuario.instrumentos;
                this.especialidades = this.usuario.especialidades;
                this.portfolio = this.usuario.portfolio;
                this.cursos = this.usuario.cursos;
                $("body").removeClass("loading");
            });
        },
        alteraFoto: function () {
            var foto = event.target.files[0];
            var form_data = new FormData();
            form_data.append("file[]", foto);
            if (foto.type.match('image.*')) {
                $.ajax({
                    url: "servicos/enviaFoto.php",
                    method: "POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: data => {
                        file = form_data.get('file[]');
                        if (FileReader && file) {
                            var fr = new FileReader();
                            fr.onload = function () {
                                meuPerfil.usuario.foto = fr.result;
                            }
                            fr.readAsDataURL(file);
                        }
                    }
                });
            } else {
                swal("Atenção", "É necessário selecionar uma imagem válida", "warning");
            }
        },
        viraProfissional: function () {
            $("body").addClass("loading");
            this.$http.get("servicos/cadastraProfissional.php").then(function () {
                meuPerfil.atualizaDados();
                $("body").removeClass("loading");
            })
        },

        fezAlteracao() {
            this.qtdCaracteresSobreVoce = this.usuario.descricao.length;
            window.onbeforeunload = async () => {
                return "";
            }
        },
        orientacoesLinkNovoProjeto() {
            switch (this.projetoPortfolioSelecionado.tipo) {
                case 'youtube':
                    return "Basta copiar o link da página do vídeo e colar aqui!";
                case 'spotify':
                    return "Copie o link da música e cole aqui (https://developer.spotify.com/documentation/widgets/generate/play-button/)"
            }
        },
        handleResize() {
            this.$forceUpdate();
        },
    },
})