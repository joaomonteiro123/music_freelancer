Vue.component("profissionais-convidar", {
    props: ["profissional"],
    template: '#profissionais-convidar-template',
    methods: {
        porcentagemEstrelas: function (profissional) {
            estrela = profissional.estrela * 20;
            return {
                width: estrela + "%"
            }
        },
        ativo: function () {
            return this.profissional.ativo == 1 ? 'ativo' : 'nao-ativo';
        },
        abrePerfil: function (id) {
            window.open("../perfilProfissional/perfilProfissional.php?id=" + id);
        },
    }
})

Vue.component("card-profissional", {
    props: ["profissional"],
    template: '#card-profissional-template',
    methods: {
        porcentagemEstrelas: function (profissional) {
            estrela = profissional.estrela * 20;
            return {
                width: estrela + "%"
            }
        },
        abrePerfil(id) {
            window.open("../perfilProfissional/perfilProfissional.php?id=" + id);
        },
    }
})

Vue.component("minutagem", {
    props: ["minutagem"],
    template: '#minutagem-template',
})


Vue.component("arquivos", {
    props: ["arquivos"],
    template: '#arquivos-template',
    methods: {
        baixaArquivo: function (id) {
        }
    },
})

meusProjetos = new Vue({
    el: "#app",
    data: {
        projeto: [],
        minutagem: [],
        instrumentos: [],
        arquivos: [],
        profissionais: [],
        profissionaisConvidar: [],
        profissionaisAtivos: [],
        novaMensagem: "",
        mensagens: [],
        profissionalMensagemAtivo: null,
        projetoEnvioAtivo: {}
    },
    created: function () {
        $("body").addClass("loading");
        this.$http.get("servicos/buscaProjeto.php").then(async function (resposta) {
            this.projeto = resposta.data[0];

            if (resposta.data[0].minutagem) {
                jsonMinutagem = JSON.parse(resposta.data[0].minutagem);
                for (i = 0; i < jsonMinutagem.obs.length; i++) {
                    key = i + 1;
                    if (jsonMinutagem.obs[i]) {
                        this.minutagem.push({ 'key': key, 'obs': jsonMinutagem.obs[i], 'tempo': jsonMinutagem.tempo[i] });
                    }
                }
            }

            this.instrumentos = this.projeto.instrumentos;
            this.arquivos = this.projeto.arquivos;

            let donoAtivo = this.projeto.idLogado == this.projeto.idDono ? 1 : 0;
            this.profissionais = (await this.$http.get("servicos/carregaProfissionais.php", { params: { donoAtivo } })).data;

            this.mensagens = this.projeto.mensagens
            if (this.projeto.idLogado != this.projeto.idDono) {
                this.profissionalMensagemAtivo = this.projeto.idLogado;
                this.projetoEnvioAtivo = this.projeto.profissionais[0];
                this.projetoEnvioAtivo.idProjeto = this.projeto.idProjeto;
                Vue.nextTick(function () {
                    $("#areaMensagens").scrollTop($("#areaMensagens")[0].scrollHeight);
                });
            }
            $("body").removeClass("loading");
        });
    },
    methods: {
        enviaProposta() {
            this.projetoEnvioAtivo.proposta = 1;
            this.projetoEnvioAtivo.aceito = 0;
            this.projetoEnvioAtivo.fl_recusado = 0;
            this.projetoEnvioAtivo.notificacaoProfissional = 0;
            this.projetoEnvioAtivo.notificacaoDono = 1;

            data = JSON.stringify(this.projetoEnvioAtivo);

            console.log(this.projetoEnvioAtivo);
            $.ajax({
                url: "servicos/editaProposta.php",
                method: "POST",
                dataType: 'json',
                data: "proposta=" + data,
                success: function (resposta) {
                    if (resposta == "OK") {
                        swal("Pronto!", "Proposta enviada com sucesso!", "success");
                    }
                }
            });
        },

        aceitarProposta() {
            this.projetoEnvioAtivo.proposta = 1;
            this.projetoEnvioAtivo.aceito = 1;
            this.projetoEnvioAtivo.notificacaoProfissional = 1;
            this.projetoEnvioAtivo.notificacaoDono = 0;

            data = JSON.stringify(this.projetoEnvioAtivo);
            $.ajax({
                url: "servicos/editaProposta.php",
                method: "POST",
                dataType: 'json',
                data: "proposta=" + data,
                success: function (resposta) {
                    if (resposta == "OK") {
                        swal("Pronto!", "Proposta aceita!", "success");
                    }
                }
            });
        },

        recusarProposta() {
            this.projetoEnvioAtivo.proposta = 0;
            this.projetoEnvioAtivo.fl_recusado = 1;
            this.projetoEnvioAtivo.notificacaoProfissional = 1;
            this.projetoEnvioAtivo.notificacaoDono = 0;

            data = JSON.stringify(this.projetoEnvioAtivo);
            $.ajax({
                url: "servicos/editaProposta.php",
                method: "POST",
                dataType: 'json',
                data: "proposta=" + data,
                success: function (resposta) {
                    if (resposta == "OK") {
                        swal("Pronto!", "Proposta recusada", "success");
                    }
                }
            });
        },

        alteraProfissionalMensagem(profissional) {
            this.profissionalMensagemAtivo = profissional.idUsuario;
            Vue.nextTick(function () {
                $("#areaMensagens").scrollTop($("#areaMensagens")[0].scrollHeight);
            });
            this.projetoEnvioAtivo = this.projeto.profissionais.filter(obj => {
                return obj.id == profissional.idProfissional
            })[0]
            this.projetoEnvioAtivo.idProjeto = this.projeto.idProjeto;
        },

        retornaProfissionalAtivo(idUsuario) {
            return idUsuario == this.profissionalMensagemAtivo ? 'ativo' : '';
        },

        enviaMensagem: function () {
            if (this.novaMensagem.length > 0) {
                mensagem = this.novaMensagem
                projeto = this.projeto.idProjeto
                remetente = this.projeto.idLogado
                if (this.projeto.idLogado == this.projeto.idDono) {
                    destinatario = this.profissionalMensagemAtivo
                } else {
                    destinatario = this.projeto.idDono
                }
                donoDoProjeto = this.projeto.idLogado == this.projeto.idDono ? 1 : 0;
                $.ajax({
                    url: "servicos/enviaMensagem.php",
                    method: "POST",
                    data: "mensagem=" + mensagem + "&remetente=" + remetente + "&projeto=" + projeto + "&destinatario=" + destinatario + "&donoDoProjeto=" + donoDoProjeto,
                    success: function (resposta) {
                        if (resposta == "erro") {
                            swal("Erro", "Um erro inesperado ocorreu", "error");
                        } else {
                            meusProjetos.mensagens = JSON.parse(resposta);
                            meusProjetos.novaMensagem = "";
                            $(".textarea-mensagem").each((index, element) => {
                                element.style.height = "38px";
                            });
                            Vue.nextTick(function () {
                                $("#areaMensagens").scrollTop($("#areaMensagens")[0].scrollHeight);
                            })
                        }
                    }
                });
            }
        },
        meusProjetos: function () {
            window.location = '../meuPerfil/meuPerfil.php';
        },
        populaProfissionais: function () {
            $("body").addClass("loading");
            this.$http.get("servicos/buscaProfissionais.php").then(function (resposta) {
                this.profissionaisConvidar = resposta.data;
                $("body").removeClass("loading");
            });
        },
        selecionar: function (profissional) {
            if (profissional.ativo == false) {
                profissional.ativo = true;
                meusProjetos.profissionaisAtivos.push(profissional.idProfissional);
            } else {
                profissional.ativo = false;
                let i = meusProjetos.profissionaisAtivos.map(item => item).indexOf(profissional.idProfissional);
                meusProjetos.profissionaisAtivos.splice(i, 1);
            }
        },
        enviaProjeto: function () {
            $.ajax({
                url: "servicos/enviaProjeto.php",
                method: "POST",
                data: "profissionais=" + this.profissionaisAtivos + "&idProjeto=" + this.projeto.idProjeto,
                success: function (resposta) {
                    if (resposta == "erro") {
                        swal("Erro", "Um erro inesperado ocorreu", "error");
                    } else {
                        meusProjetos.profissionaisAtivos = [];
                    }
                }
            });
            location.reload();
        },
        numeroArquivos(categoria) {
            return (this.arquivos.filter(el => {
                return el.categoria === categoria;
            })).length;
        },
        atualizaTamanhoMensagem() {
            $(".textarea-mensagem").each((index, element) => {
                element.style.height = "";
                element.style.height = element.scrollHeight + "px";
            });
        }
    },
})