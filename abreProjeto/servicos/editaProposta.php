<?php
include "../../sessao.php";
$proposta = json_decode($_POST['proposta']);

$query = "UPDATE projeto_envio SET fl_aceito = (?), fl_proposta = (?), fl_recusado = (?), valor = (?),
                                fl_notificacao_profissional = (?), fl_notificacao_dono = (?)
                WHERE id = (?)";

if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));  
}

if (!$query->bind_param("iiisiii", $proposta->aceito, $proposta->proposta, $proposta->fl_recusado,
                                $proposta->valor, $proposta->notificacaoProfissional,
                                $proposta->notificacaoDono, $proposta->key)){

        die('bind_param() failed: ' . htmlspecialchars($query->error));
}

if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
}


$conexao->close();
echo json_encode("OK");
?>