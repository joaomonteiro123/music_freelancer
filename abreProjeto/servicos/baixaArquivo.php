<?php
include "../../sessao.php";
$idUsuario = $_SESSION['idUsuario'];
$idArquivo = $_GET['id'];

$query = "SELECT * FROM arquivosprojeto a INNER JOIN projeto p ON p.id = a.id_projeto
WHERE p.id_usuario = (?) AND a.id = (?)";
$query = $conexao->prepare($query);
$query -> bind_param('ii',$idUsuario, $idArquivo);

if ($query->execute() === TRUE) {
    $query = $query -> get_result();
    if ($query->num_rows < 1){
        $query = "SELECT * FROM arquivosprojeto a INNER JOIN projeto_envio p ON
        p.id_projeto = a.id_projeto WHERE p.id_profissional = (?) AND a.id = (?)";
        $query = $conexao->prepare($query);
        $query -> bind_param('ii',$idUsuario, $idArquivo);
        if ($query->execute() === TRUE) {
            $query = $query -> get_result();
            if ($query->num_rows < 1){
                echo ("Você não possui autorização para visualizar essa página. Se acha que isso é um erro, favor contatar o suporte.");
            }else{
                $query = "SELECT FROM arquivosprojeto a WHERE a.id = (?)";
                $query = $conexao->prepare($query);
                $query -> bind_param('i', $idArquivo);
                if ($query->execute() === TRUE) {
                    $res = $query->get_result();
                    $n = $res -> fetch_assoc();
                    $tamanho = $n['tamanho'];
                    $tipo = utf8_encode($n['tipo']);
                    $nome = utf8_encode($n['nome']);
                    header("Content-length: $tamanho");
                    header("Content-type: $tipo");
                    header("Content-Disposition: attachment; filename=$nome");
                    ob_clean();
                    flush();
                    echo base64_decode($n['arquivo']);
                }else{
                    echo ("Você não possui autorização para visualizar essa página. Se acha que isso é um erro, favor contatar o suporte.");
                }
            }
        }
    }else{
        $query = "SELECT * FROM arquivosprojeto a WHERE a.id = (?)";
        $query = $conexao->prepare($query);
        $query -> bind_param('i', $idArquivo);
        if ($query->execute() === TRUE) {
            $res = $query->get_result();
            $n = $res -> fetch_assoc();
            $tamanho = $n['tamanho'];
            $tipo = utf8_encode($n['tipo']);
            $nome = utf8_encode($n['nome']);
            header("Content-length: $tamanho");
            header("Content-type: $tipo");
            header("Content-Disposition: attachment; filename=$nome");
            ob_clean();
            flush();
            echo ($n['arquivo']);
        }else{
            echo ("Você não possui autorização para visualizar essa página. Se acha que isso é um erro, favor contatar o suporte.");
        }
    }
}else {
    echo ("Você não possui autorização para visualizar essa página. Se acha que isso é um erro, favor contatar o suporte.");
}

$conexao->close();
exit;
?>