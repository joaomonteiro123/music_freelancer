<?php
include "../../sessao.php";

$idProjeto = $_SESSION['idProjeto'];
$query = "SELECT
            p.nome_artistico as nome, p.id as id, u.foto, u.id as idUsuario,
            GROUP_CONCAT(i.nome SEPARATOR ', ') AS instrumento_nome,
            GROUP_CONCAT(i.id SEPARATOR ', ') AS instrumento_id,
            GROUP_CONCAT(e.descricao SEPARATOR ', ') AS especialidades_nome,
            GROUP_CONCAT(e.id SEPARATOR ', ') AS especialidades_id
        FROM profissional p
        LEFT JOIN profissional_instrumentos pi 
            ON pi.id_profissional = p.id
        LEFT JOIN instrumentos i 
            ON (pi.id_instrumentos = i.id)
        LEFT JOIN profissional_especialidades pes
            ON pes.id_profissional = p.id
        LEFT JOIN especialidades e
            ON e.id = pes.id_especialidades
        INNER JOIN usuario u
            ON u.id = p.id_usuario
            AND u.id != (?)
        WHERE p.id NOT IN (
            SELECT id_profissional
            FROM projeto_envio pe 
            WHERE pe.id_projeto = (?)
        )
        GROUP BY p.id";
$query = $conexao->prepare($query);
$query ->bind_param('ii',$idLogado, $idProjeto);
$query->execute();

$res = $query->get_result();
$j=0;

$i = 0;
$profissional = array();
while ($n = $res -> fetch_assoc()) {
    $j=$j+0.7;
    $estrela=2.0+$j;
    $orcamento = 1500 - 300 * $j;
    $orcamento = number_format($orcamento, 2, ',', '.');
    $profissional[$i]['nomeProfissional'] = utf8_encode($n['nome']);
    $profissional[$i]['descricao'] = utf8_encode($n['descricao']);
    $profissional[$i]['idProfissional'] = $n['id'];
    $profissional[$i]['idInstrumento'] = $n['instrumento_id'];
    $profissional[$i]['nomeInstrumento'] = utf8_encode($n['instrumento_nome']);
    $profissional[$i]['idEspecialidade'] = $n['especialidades_id'];
    $profissional[$i]['nomeEspecialidade'] = utf8_encode($n['especialidades_nome']);
    $profissional[$i]['estrela'] = $estrela;
    $profissional[$i]['orcamento'] = $orcamento;
    $profissional[$i]['ativo'] = false;

    if ( $n['foto'] == "" ){
        $n['foto'] = "../images/generic_user.png";
    }else{
        $n['foto'] = "../servicos/imagemUsuario.php?id=".$n['idUsuario'];
    }   
    $profissional[$i]['foto'] = $n['foto'];
    
    $i = $i + 1;
}

$conexao->close();
echo json_encode($profissional);
?>