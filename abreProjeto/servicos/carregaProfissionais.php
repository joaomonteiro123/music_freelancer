<?php
include "../../sessao.php";
$idProjeto = $_SESSION['idProjeto'];

if ($_GET['donoAtivo']) {
    $query = "SELECT p.nome_artistico as nome, p.id as id, u.foto, u.id as idUsuario, pe.id as idProjetoEnvio
            FROM profissional p
            INNER JOIN projeto_envio pe
                ON pe.id_profissional = p.id
                AND pe.fl_aceito>=
                    (SELECT SUM(pe2.fl_aceito)
                    FROM projeto_envio pe2
                    WHERE pe2.id_projeto = (?))
            INNER JOIN usuario u ON u.id = p.id_usuario
            WHERE pe.id_projeto = (?)";

    $query = $conexao->prepare($query);
    $query -> bind_param('ii', $idProjeto, $idProjeto);
} else {
    $query = "SELECT u.primeiro_nome as nome, null as id, u.foto, u.id as idUsuario, pe.id as idProjetoEnvio
            FROM usuario u
            INNER JOIN projeto_envio pe
                ON pe.id_projeto in (SELECT id_usuario FROM projeto WHERE id = (?))
                AND pe.fl_aceito>=
                    (SELECT SUM(pe2.fl_aceito)
                    FROM projeto_envio pe2
                    WHERE pe2.id_projeto = (?))
            WHERE pe.id_projeto = (?)";

    $query = $conexao->prepare($query);
    $query -> bind_param('iii',$idLogado,$idProjeto, $idProjeto);
}

$query->execute();
$res = $query->get_result();

$j=0;
$i = 0;
$profissional = array();
while ($n = $res -> fetch_assoc()) {
    $j=$j+0.7;
    $estrela=2.0+$j;
    $orcamento = 1500 - 300 * $j;
    $orcamento = number_format($orcamento, 2, ',', '.');
    $profissional[$i]['nomeProfissional'] = utf8_encode($n['nome']);
    $profissional[$i]['idProfissional'] = $n['id'];
    $profissional[$i]['idUsuario'] = $n['idUsuario'];
    $profissional[$i]['idProjetoEnvio'] = $n['idProjetoEnvio'];

    $idUsuario = $n['idUsuario'];
    if ( $n['foto'] == "" ){
        $n['foto'] = "../images/generic_user.png";
    }else{
        $n['foto'] = "../servicos/imagemUsuario.php?id=".$idUsuario;
    }   
    $profissional[$i]['foto'] = $n['foto'];
    
    $i = $i + 1;
}

$conexao->close();
echo json_encode($profissional);
?>