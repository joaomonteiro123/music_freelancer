<?php
include "../../sessao.php";
$idProjeto = $_POST['idProjeto'];
$idUsuario = $_SESSION['idUsuario'];
$profissionais = explode(',', $_POST['profissionais']);

$query = "SELECT * FROM projeto p WHERE p.id_usuario = (?) AND p.id = (?)";
$query = $conexao->prepare($query);
$query->bind_param("ii", $idUsuario, $idProjeto);
$query->execute();
$query = $query -> get_result();
if ($query->num_rows < 1){
    $conexao->close();
    echo "erro";
}else{
    foreach($profissionais as $profissional){
        $query = "INSERT INTO `projeto_envio` (`id_profissional`, `id_projeto`, `fl_aceito`)
                VALUES ((?), (?), 0)";
        $query = $conexao->prepare($query);
        $query->bind_param("ii", $profissional, $idProjeto);
        $query->execute();
    }
}
$conexao->close();
exit;
?>