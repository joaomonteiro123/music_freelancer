<?php
$idUsuario = $_SESSION['idUsuario'];
$_SESSION['idProjetoInstrumentista'] = null;
$idProjeto = filter_input(INPUT_GET,"id",FILTER_SANITIZE_STRING);

$query = "SELECT * FROM projeto WHERE id = (?) AND id_usuario = (?) AND fl_ativo = 1";
$query = $conexao->prepare($query);
$query -> bind_param('ii',$idProjeto, $idUsuario);
if ($query->execute() === TRUE) {
    $query = $query -> get_result();
    if ($query->num_rows < 1){
        $query = "SELECT * FROM projeto_envio pe INNER JOIN profissional p ON p.id = pe.id_profissional
                    AND p.id_usuario = (?) WHERE pe.id_projeto = (?)
                    AND pe.fl_aceito>= 
                        (SELECT SUM(pe2.fl_aceito)
                        FROM projeto_envio pe2
                        WHERE pe2.id_projeto = (?))";
        $query = $conexao->prepare($query);
        $query -> bind_param('iii', $idUsuario, $idProjeto, $idProjeto);
        if ($query->execute() === TRUE) {
            $query = $query -> get_result();
            if ($query->num_rows < 1){
                $conexao->close();
                header('Location: ../meuPerfil/meuPerfil.php'); 
            }
        }else{
            echo "erro";
        } 
    }
} else {
    echo "erro";
}
$_SESSION['idProjeto'] = $idProjeto;
$conexao->close();
?>