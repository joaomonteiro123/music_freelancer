<?php
include "../../sessao.php";
$id = $_SESSION['idProjeto'];
$query = "SELECT p.observacoes_gerais, p.metronomo, p.referencia_link, p.minutagem,
        p.outros_instrumentos, p.fl_VST, pp.nome, pp.data_criacao as dataa,
        pp.id_usuario as idDono, SUM(pe.fl_aceito) as aceito, pp.id as id_projeto
        FROM projeto_instrumentista p 
        INNER JOIN projeto pp ON pp.id = p.id_projeto
        LEFT JOIN projeto_envio pe ON pe.id_projeto = pp.id
        WHERE p.id_projeto = (?) GROUP BY p.observacoes_gerais, p.metronomo, p.referencia_link, p.minutagem,
        p.outros_instrumentos, p.fl_VST, pp.nome, pp.data_criacao, pp.id_usuario, pp.id, pe.id";

if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}

if (!$query -> bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}

if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
    $conexao->close();
}

$res = $query->get_result();
$n = $res -> fetch_assoc();
$projeto[0]['nome'] = utf8_encode($n['nome']);
$projeto[0]['observacoes'] = utf8_encode($n['observacoes_gerais']);
$projeto[0]['metronomo'] = $n['metronomo'];
$projeto[0]['referencia'] = utf8_encode($n['referencia_link']);
$projeto[0]['minutagem'] = utf8_encode($n['minutagem']);  
if($projeto[0]['minutagem'] == '[]'){
    $projeto[0]['minutagem'] = "";
}
$projeto[0]['outrosInstrumentos'] = utf8_encode($n['outros_instrumentos']);
$projeto[0]['VST'] = $n['fl_VST'];
$projeto[0]['data'] = date("d-m-Y", strtotime($n['dataa']));
$projeto[0]['idDono'] = $n['idDono'];
$projeto[0]['aceito'] = $n['aceito'];
$projeto[0]['idProjeto'] = $n['id_projeto'];


if ($_SESSION['idUsuario'] == $projeto[0]['idDono']) {
    $query = "UPDATE projeto_envio SET fl_notificacao_dono = 0 WHERE id_projeto = (?)";

    if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));  
    }

    if (!$query->bind_param("i", $id)){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
    }

    if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
    }
} else {
    $query = "UPDATE projeto_envio SET fl_notificacao_profissional = 0
            WHERE id_projeto = (?) AND id_profissional in (
                SELECT id FROM profissional WHERE id_usuario = (?)
            )";

    if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));  
    }

    if (!$query->bind_param("ii", $id, $_SESSION['idUsuario'])){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
    }

    if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
    }
}


$query = "SELECT DISTINCT i.id, i.nome FROM projeto_instrumentista_intrumentos pi
    INNER JOIN instrumentos i ON i.id = pi.id_instrumentos INNER JOIN
    projeto_instrumentista p ON p.id = pi.id_projeto_instrumentista WHERE p.id_projeto = (?)";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
    $conexao->close();
}
$res = $query->get_result();
$instrumentos = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $instrumentos[$i]['nome'] = utf8_encode($n['nome']);
    $instrumentos[$i]['id'] = $n['id']; 
    $i = $i + 1;
}

$projeto[0]['instrumentos'] = $instrumentos;


$query = "SELECT ap.nome,
                ap.id,
                ap.tamanho,
                ap.categoria
        FROM `arquivosprojeto` ap
        WHERE ap.id_projeto = (?)";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('i',$id)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
    $conexao->close();
}
$res = $query->get_result();
$arquivos = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $arquivos[$i]['nome'] = utf8_encode($n['nome']);
    $arquivos[$i]['id'] = $n['id']; 
    $arquivos[$i]['tamanho'] = number_format((float)$n['tamanho']/1048576, 2, '.', ''); 
    $arquivos[$i]['categoria'] = $n['categoria']; 
    $i = $i + 1;
}
$projeto[0]['arquivos'] = $arquivos;

if ($_SESSION['idUsuario'] == $projeto[0]['idDono']){
    $query = "SELECT * FROM `projeto_envio` pe
            WHERE pe.id_projeto = (?)
            AND pe.fl_aceito >= 
                (SELECT SUM(pe2.fl_aceito)
                FROM projeto_envio pe2 
                WHERE pe2.id_projeto = (?))";
    
    if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));
    }
    
    if (!$query -> bind_param('ii', $id, $id)){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
    }   
} else {
    $query = "SELECT * FROM `projeto_envio` pe
            WHERE pe.id_projeto = (?)
            AND pe.fl_aceito >= 
                (SELECT SUM(pe2.fl_aceito)
                FROM projeto_envio pe2 
                WHERE pe2.id_projeto = (?))
            AND id_profissional IN (SELECT id FROM profissional WHERE id_usuario = (?))";
    
    if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));
    }
    
    if (!$query -> bind_param('iii', $id, $id, $_SESSION['idUsuario'])){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
    }   
}

if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
    $conexao->close();
}

$res = $query->get_result();
$profissionais = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $profissionais[$i]['key'] = $n['id'];
    $profissionais[$i]['id'] = $n['id_profissional']; 
    $profissionais[$i]['aceito'] = $n['fl_aceito']; 
    $profissionais[$i]['proposta'] = $n['fl_proposta']; 
    $profissionais[$i]['fl_recusado'] = $n['fl_recusado']; 
    $profissionais[$i]['valor'] = $n['valor'];
    $i = $i + 1;
}

$projeto[0]['profissionais'] = $profissionais;

$projeto[0]['idLogado'] = $_SESSION['idUsuario'];

if( $projeto[0]['idLogado'] == $projeto[0]['idDono']){
    $query = "SELECT * FROM `negociacao` WHERE id_projeto = (?) ORDER BY horario ASC";
    if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));
    }
    if (!$query -> bind_param('i',$id)){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
    }
    if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
    }
    $res = $query->get_result();
    $mensagens = array();
    $i = 0;
    while ($n = $res -> fetch_assoc()) {
        $mensagens[$i]['remetente'] = $n['id_remetente'];
        $mensagens[$i]['destinatario'] = $n['id_destinatario']; 
        $mensagens[$i]['data'] = $n['horario'];
        $mensagens[$i]['texto'] = utf8_encode($n['mensagem']);  
        $i = $i + 1;
    }
}else{
    $query = "SELECT * FROM `negociacao` WHERE id_projeto = (?) AND
             (id_remetente = (?) OR id_destinatario = (?)) ORDER BY horario ASC";
    if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));
    }
    if (!$query -> bind_param('iii',$id, $projeto[0]['idLogado'], $projeto[0]['idLogado'])){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
    }
    if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
    }
    $res = $query->get_result();
    $mensagens = array();
    $i = 0;
    while ($n = $res -> fetch_assoc()) {
        $mensagens[$i]['remetente'] = $n['id_remetente'];
        $mensagens[$i]['destinatario'] = $n['id_destinatario']; 
        $mensagens[$i]['data'] = $n['horario'];
        $mensagens[$i]['texto'] = utf8_encode($n['mensagem']);  
        $i = $i + 1;
    }
}
$projeto[0]['mensagens'] = $mensagens;

$conexao->close();
echo json_encode($projeto);
?>