<?php
include "../../sessao.php";
$projeto = $_POST['projeto'];
$mensagem = utf8_decode($_POST['mensagem']);
$remetente = $_POST['remetente'];
$destinatario = $_POST['destinatario'];
$donoDoProjeto = $_POST['donoDoProjeto'];

if($donoDoProjeto){
        $notificacao_profissional = 1;
        $notificacao_dono = 0;
        $idProfissional = $destinatario;
} else {
        $notificacao_profissional = 0;
        $notificacao_dono = 1;
        $idProfissional = $remetente;
}

$query = "UPDATE projeto_envio SET fl_notificacao_profissional = (?), fl_notificacao_dono = (?)
         WHERE id_projeto = (?) AND id_profissional in (SELECT id FROM profissional WHERE id_usuario = (?))";
         
if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));  
}
if (!$query->bind_param("iiii", $notificacao_profissional, $notificacao_dono, $projeto, $idProfissional)){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
}


$query = "INSERT INTO `negociacao` (`id`, `id_projeto`, `id_remetente`, `id_destinatario`, `horario`,
        `mensagem`) VALUES (NULL, (?), (?), (?), CURRENT_TIMESTAMP, (?));";
if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));  
}
if (!$query->bind_param("iiis", $projeto, $remetente, $destinatario, $mensagem)){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
}

$query = "SELECT * FROM `negociacao`
        WHERE id_projeto = (?) AND (id_remetente = (?) OR id_destinatario = (?))
        ORDER BY horario ASC";
if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));  
}
if (!$query->bind_param("iii", $projeto, $remetente, $remetente)){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
}
$res = $query->get_result();

$mensagens = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
        $mensagens[$i]['remetente'] = $n['id_remetente'];
        $mensagens[$i]['destinatario'] = $n['id_destinatario']; 
        $mensagens[$i]['data'] = $n['horario'];
        $mensagens[$i]['texto'] = utf8_encode($n['mensagem']);  
        $i = $i + 1;
}

$conexao->close();
echo json_encode($mensagens);
?>