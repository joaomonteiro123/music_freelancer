<?php
include "../sessao.php";
include "servicos/confereUsuario.php";
?>

<HTML>
<HEAD>
    <meta charset="utf-8">
    <TITLE>Musitray - Sua música a um clique</TITLE>
    <link rel="stylesheet" href="../bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/estilo.css">
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/combobox.js"></script>
    <script src="../js/jquery.filterizr-with-jquery.min.js"></script>
    <script src="../js/jquery.filterizr.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-resource.js"></script>
    <script src="../js/vue-router.js"></script>
    <script src="../js/sweetalert.min.js"></script>
</HEAD>

<script  type="text/x-template" id="card-profissional-template">
    <div :id="profissional.idProfissional" class="col-2">
        <div class="card-profissional-sem-selecao pl-0 pr-0" align="center">   
            <div class="foto-profissional">
                <img :src="profissional.foto">
            </div>
            <div class="row">
                <div class="col-12 mt-2">
                    <h3>{{profissional.nomeProfissional}}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <small class="text-muted">Instrumentos</small>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>{{profissional.nomeInstrumento}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <small class="text-muted">Especialidades</small>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>{{profissional.nomeEspecialidade}}</p>
                </div>
            </div>
            <button type="button" class="btn btn-quadrado btn_criar mb-2 mt-0" @click="abrePerfil(profissional.idProfissional)">Ver perfil</button>
        </div>
    </div>
</script>

<script  type="text/x-template" id="minutagem-template">
    <div class="row mb-2">
        <div class="col-4">
            <textarea
                type="text"
                class="form-control input disabled textarea w-100 bg-light"
                :value="minutagem.tempo"
                disabled>
            </textarea>
        </div>
        <div class="col-8">
            <textarea
                type="text"
                class="form-control input disabled textarea w-100 bg-light"
                :value="minutagem.obs"
                disabled>
            </textarea>
        </div>
    </div>
</script>

<script  type="text/x-template" id="arquivos-template">
    <div class="row">
        <div class="col-12">
            <a :href="'servicos/baixaArquivo.php?id='+ arquivos.id" target="_blank">{{ arquivos.nome }} - {{ arquivos.tamanho }} Mb</a>
            <hr>
        </div>
    </div>
</script>

<script type="text/x-template" id="profissionais-convidar-template">
    <div :data-category="[profissional.idInstrumento, ativo()]" :data-orcamento="profissional.orcamento" :data-estrelas="profissional.estrela" class="filtr-item col-3">
        <div :id="profissional.idProfissional" class="card-profissional pl-0 pr-0" :class="{ 'card-ativo': profissional.ativo == 1 }" align="center">    
            <div style="overflow:hidden; padding: 0;">
                <img class="img_card_profissional m-4" :src="profissional.foto">
            </div>
            <div class="row">
                <div class="col-12">
                    <h3>{{profissional.nomeProfissional}}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <small class="text-muted">Instrumentos</small>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>{{profissional.nomeInstrumento}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <small class="text-muted">Especialidades</small>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>{{profissional.nomeEspecialidade}}</p>
                </div>
            </div>
            <hr>
            <button type="button" class="btn btn-quadrado btn-criar mb-2 w-50"
                @click="abrePerfil(profissional.idProfissional)">Perfil
            </button>
        </div>
    </div>
</script>


<BODY>      
<?php 
    include "../cabecalho/cabecalho.php"
?>
<br>
<div class="container" id="app">
    <div class="row">
        <div class="col-12">
            <label class="texto-principal grande">Detalhes do Projeto</label>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p>{{projeto.nome || "Esse projeto não possui um nome"}}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <label class="texto-principal medio">Observações<label>
        </div>
    </div>
    <div v-if="projeto.observacoes || minutagem.length > 0">
        <div v-if="projeto.observacoes">
            <!-- <div class="row">
                <div class="col-12">
                    <label class="texto-principal small">Observações gerais<label>
                </div>
            </div> -->
            <div class="row">
                <div class="col-12">
                    <div>
                        {{projeto.observacoes}}
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div v-if="minutagem.length > 0">
            <!-- <div class="row">
                <div class="col-12">
                    <label class="texto-principal small">Observações específicas<label>
                </div>
            </div> -->
            <div class="row">
                <div class="col-4 text-center"><label class="texto-principal small">Minutagem</label></div>
                <div class="col-8 text-center"><label class="texto-principal small">Observação específica</label></div>
            </div>
            <div class="row">
                <div class="col-12">
                    <minutagem
                        v-for="minutagem in minutagem"
                        v-bind:key="minutagem.key"
                        :minutagem="minutagem"
                    ></minutagem>
                </div>
            </div>
        </div>    
    </div>
    <div v-else>
        Esse projeto não possui nenhuma observação
    </div>
    <hr>

    <div class="row">
        <div class="col-6">
            <label class="texto-principal medio">Instrumentos<label>
        </div>
        <div class="col-6">
            <label class="texto-principal medio">Arquivos e Observações<label>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div v-if="instrumentos.length > 0">
                <div class="row">
                    <div class="col-12">
                        <textarea
                            type="text"
                            class="form-control input disabled textarea w-100 bg-light"
                            :value="instrumentos.map(e => e.nome).join(', ') + ', ' + projeto.outrosInstrumentos" disabled>
                        </textarea>                                                                                                                                                 
                    </div>
                </div>
            </div>
            <div v-else>Não existem instrumentos cadastrados para esse projeto.</div>
        </div>

        <div class="col-6" v-if="arquivos.length > 0">
            <div class="row">
                <div class="col-6">
                    <label v-if="numeroArquivos('cifra') > 0" class="texto-principal small">Cifra</label>
                </div>
                <div class="col-6">
                    <label v-if="numeroArquivos('musica') > 0" class="texto-principal small">Música</label>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <arquivos
                    v-for="(arquivos, index) in arquivos"
                    v-if="arquivos.categoria == 'cifra'"
                    v-bind:key="arquivos.id"
                    :arquivos="arquivos"
                    @click="baixaArquivo(arquivos.id)"
                    ></arquivos>
                </div>
                <div class="col-6">
                    <arquivos
                    v-for="(arquivos, index) in arquivos"
                    v-if="arquivos.categoria == 'musica'"
                    v-bind:key="arquivos.id"
                    :arquivos="arquivos"
                    ></arquivos>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <label v-if="numeroArquivos('referencia') > 0" class="texto-principal small">Referências</label>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <arquivos
                        v-for="(arquivos, index) in arquivos"
                        v-if="arquivos.categoria == 'referencia'"
                        v-bind:key="arquivos.id"
                        :arquivos="arquivos"
                        @click="baixaArquivo(arquivos.id)"
                    ></arquivos>
                    <div class="row" v-if="projeto.referencia">
                        <div class="col-12">
                            {{projeto.referencia}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6" v-else>
            Não foram adicionados arquivos de referência para esse projeto.
        </div>
    </div>
    

    <br>
    <br>



    <div v-if="projeto.idLogado == projeto.idDono && !projeto.dono > 0">
        <div class="row">
            <div class="col-12">
                <label class="texto-principal grande">Profissionais convidados</label>
                <button type="button" class="btn btn-quadrado btn_criar float-right"
                    href="#modalConvidarProfissional" data-target="#modalConvidarProfissional" data-toggle="modal"
                    @click="populaProfissionais()"
                    >Convidar mais profissionais
                </button>
            </div>
            <div class="col-4">
            </div>
        </div>
        <div v-if="profissionais.length > 0" class="row">
            <card-profissional
                v-for="profissional in profissionais"
                v-bind:key="profissional.idProfissional"
                :profissional="profissional"
            ></card-profissional>
        </div>
        <div v-else>
            Nenhum profissional foi convidado para esse projeto
        </div>
    </div>
    <br>
    <div v-if="(profissionais.length != 0 && projeto.idLogado == projeto.idDono) || projeto.idLogado != projeto.idDono" class="row">
        <div class="col-12">
            <label class="texto-principal medio">Área de negociação<label>
        </div>
        <div class="row" style="max-width: 100%; min-width: 100%;">
            <div class="col-4 border bg-white p-4" v-if="projeto.idLogado == projeto.idDono">
                <div class="row">
                    <div class="col-12 mb-3">
                        <label class="texto-principal pequeno">Profissionais</label>
                        <div class="row" v-for="(profissional, index) in profissionais" v-bind:key="index">
                            <div class="col-12 profissional-chat border rounded" :class="retornaProfissionalAtivo(profissional.idUsuario)" @click="alteraProfissionalMensagem(profissional)">
                                <div class="row">
                                    <div class="col-3 p-0">
                                        <img class="w-100" :src="profissional.foto">
                                    </div>
                                    <div class="col-9 nome-profissional-chat">
                                        <b>{{profissional.nomeProfissional}}</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-8 border bg-white p-4">
                <div v-if="profissionalMensagemAtivo">
                    <div class="row">
                        <div class="col-12" v-if="projetoEnvioAtivo.aceito && projeto.idLogado == projeto.idDono">
                            <p>Você aceitou a proposta desse profissional pelo valor de R$ {{projetoEnvioAtivo.valor}}.</p>
                        <hr>
                        </div>
                        <div class="col-12" v-else-if="projetoEnvioAtivo.proposta && projeto.idLogado == projeto.idDono && !projetoEnvioAtivo.fl_recusado">
                            <p>Esse profissional deseja executar o seu projeto. Caso você aceite, os outros profissionais para os quais você enviou o projeto não poderão fazer propostas</p>
                            <p>R$ {{projetoEnvioAtivo.valor}}</P>
                            <button class="btn btn-quadrado btn_criar" @click="aceitarProposta()">Aceitar</button>
                            <button class="btn btn-quadrado cancelar" @click="recusarProposta()">Recusar</button>
                            <hr>
                        </div>
                        <div class="col-12 mb-3 message_area" id="areaMensagens">
                            <div v-for="mensagem in mensagens">
                                <div v-if="mensagem.destinatario == profissionalMensagemAtivo || mensagem.remetente == profissionalMensagemAtivo">
                                    <div class="incoming_msg" v-if="mensagem.destinatario == projeto.idLogado">
                                        <div class="received_msg">
                                            <div class="received_withd_msg">
                                                <p>{{mensagem.texto}}</p>
                                                <span class="small_chat">{{mensagem.data}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outgoing_msg" v-else>
                                        <div class="sent_msg">
                                            <p>{{mensagem.texto}}</p>
                                            <span class="small_chat">{{mensagem.data}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="mensagens.length == 0" class="text-center">
                                <p>Não existem mensagens para serem exibidas</p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <textarea rows="1" maxlength="10000" v-model="novaMensagem" type="text" class="form-control border-0 textarea-mensagem" placeholder="Digitar mensagem..." @keyup="atualizaTamanhoMensagem()"></textarea>
                                <span class="input-group-btn">
                                <button class="btn btn-quadrado btn_criar" type="button" @click="enviaMensagem()">Enviar</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center" v-else>
                    <p>Clique em um profissional ao lado para iniciar a conversa<p>
                </div>
            </div>
            <div class="col-4 border bg-white p-4" v-if="projeto.idLogado != projeto.idDono">
                <div class="row" v-if="projetoEnvioAtivo.proposta && projetoEnvioAtivo.aceito">
                    <div class="col-12 mb-3">
                        <p>A sua proposta foi aceita pelo valor de R$ {{projetoEnvioAtivo.valor}}!</p>
                    </div>
                </div>
                <div class="row" v-else-if="projetoEnvioAtivo.proposta && !projetoEnvioAtivo.fl_recusado">
                    <div class="col-12 mb-3">
                        <p>Você enviou uma proposta para esse projeto, aguarde o dono do projeto responder.</p>
                        <p>R$ {{projetoEnvioAtivo.valor}}</P>
                    </div>
                </div>
                <div class="row" v-else-if="projetoEnvioAtivo.fl_recusado">
                    <div class="col-12 mb-3">
                        <p>A sua proposta foi recusada,
                            mas você pode enviar uma nova proposta</p>
                        <input type="number" v-model="projetoEnvioAtivo.valor">
                        <button class="btn btn-quadrado btn_criar" @click="enviaProposta()">Enviar</button>
                    </div>
                </div>
                <div class="row" v-else-if="!projetoEnvioAtivo.proposta">
                    <div class="col-12 mb-3">
                        <p>Você pode enviar uma proposta para esse projeto</p>
                        <input type="number" v-model="projetoEnvioAtivo.valor">
                        <button class="btn btn-quadrado btn_criar" @click="enviaProposta()">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col=md-12">
            <button type="button" class="btn btn-quadrado cinza" @click="meusProjetos()">Voltar aos meus projetos</button>
        </div>
    </div>
    <br>
    <br>

    <div class="container">
        <div class="modal fade" id="modalConvidarProfissional" role="dialog">
            <div class="modal-dialog w-75" style="max-width: 100%;">                              
                <div class="modal-content quina-viva">
                    <div class="modal-header">
                        <label class="texto-principal medio">Convidar novos profissionais</label>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <profissionais-convidar
                                v-for="profissional in profissionaisConvidar"
                                v-bind:key="profissionaisConvidar.id"
                                @click.native="selecionar(profissional)"
                                :profissional="profissional"
                            ></profissionais-convidar>
                        </div>
                        <div class="text-center mb-2" v-if="profissionaisConvidar.length == 0">
                            Não existem novos profissionais para serem convidados!
                        </div>
                    </div>
                    <div v-if="profissionaisConvidar.length > 0" class="modal-footer">
                        <button type="button" class="btn btn-quadrado btn_criar w-100" @click="enviaProjeto()">Enviar</button> 
                        <button type="button" class="btn btn-quadrado cancelar w-100" class="close" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
    include "../rodape/rodape.php"
?>


<script src="../js/popper.min.js" crossorigin="anonymous"></script>
<script src="abreProjeto.js"></script>
<script src="../bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
</BODY>
</HTML>