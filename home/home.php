<?php
include "../sessao.php";
?>

<HTML>
<HEAD>
<meta charset="utf-8">
    <TITLE>Musitray - Sua música a um clique</TITLE>
    <link rel="stylesheet" href="../bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/estilo.css">
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/combobox.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
</HEAD>

<BODY class="text-center">      
<?php 
    include "../cabecalho/cabecalho.php"
?>  
<div class="fundo-home">
    <div class="div-home">
        <img class="logo-home" src="../images/logo_clara_slogan.png">
        <p class="texto-home">Na plataforma Musiway você contrata profissionais para transformarem com qualidade sua ideia musical em realidade! Pode ser apenas um instrumento para ser inserido na sua música, toda a produção ou mesmo a mixagem; são vários serviços disponíveis. Aqui você encontra pessoas com as habilidades necessárias para atenderem sua demanda da melhor forma possível! Comunicação e pagamento pela própria plataforma, garantindo sua segurança. E aí, vamos começar?</p>
        <div class="row">
            <div class="col-6">
                <button onclick="window.location='../novoProjeto/novoProjeto.php'" type="button" class="btn btn-quadrado w-100">Publicar um projeto</button>
            </div>
            <div class="col-6">
                <button onclick="window.location='../meuPerfil/meuPerfil.php?profissional=1'" type="button" class="btn btn-quadrado w-100">Quero ser um profissional</button>
            </div>
        </div>
    </div>
<div>
<div class='blank-content-90'></div>

<?php 
    include "../rodape/rodape.php"
?>

<script src="../js/popper.min.js" crossorigin="anonymous"></script>
<script src="home.js" crossorigin="anonymous"></script>
<script src="../bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
</BODY>
</HTML>