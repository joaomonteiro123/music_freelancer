(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                if (form.checkValidity() === false) {
                    event.stopPropagation();
                    form.classList.add('was-validated');
                } else {
                    form.classList.add('was-validated');
                    $.ajax({
                        url: "servicos/signupAsProfessional.php",
                        type: "post",
                        data: $("#frm_cadastro").serialize(),
                        success: function (resultado) {
                            if (resultado == 'OK') {
                                window.location.replace("../meuPerfil/meuPerfil.php?profissional=1");
                            } else {
                                swal("Erro", resultado, "error");
                            }
                        }
                    })
                }
            }, false);
        });
    }, false);
})();

$(document).on({
    ajaxStart: function () { $("body").addClass("loading"); },
    ajaxStop: function () { $("body").removeClass("loading"); }
});

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


$(document).ready(function () {
    let url_string = window.location.href;
    let url = new URL(url_string);
    let email = url.searchParams.get("email");
    if (validateEmail(email)) {
        $("#email").val(email)
    }
});