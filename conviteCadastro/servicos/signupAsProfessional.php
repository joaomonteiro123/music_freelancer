<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '../../vendor/autoload.php';

try {
    include "../../servicos/conectasql.php";
    
    //Checa o tamanho mínimo da senha
    if (strlen($_POST['senha']) < 4){
        exit("A senha precisa de, no mínimo, 4 caracteres!");
    }

    //Checa se as senhas são iguais
    if ($_POST['senha'] != $_POST['senha2']){
        exit("As senhas informadas são diferentes!");
    }

    if (!$_POST['email']){
        exit("É necessário informar um e-mail válido");
    }

    // Checa se email já existe no banco
    $query = "SELECT * FROM usuario u WHERE u.email = (?)";
    $query = $conexao->prepare($query);
    $query->bind_param("s",$_POST['email']);
    $query->execute();
    $resultado = $query->get_result();
    if ($resultado -> num_rows > 0){
        exit("E-mail já cadastrado!");
    }
    $query->close();

    // Cadastra o usuário
    $senha = utf8_decode(mysqli_real_escape_string($conexao, $_POST["senha"])); 
    $senha = md5($senha); 
    $primeiroNome = utf8_decode($_POST['primeiroNome']);
    $ultimoNome = utf8_decode($_POST['ultimoNome']);
    $email = utf8_decode($_POST['email']); 

    $query = "INSERT INTO usuario (senha, primeiro_nome, ultimo_nome, email) VALUES (?, ?, ?, ?)";
    $query = $conexao->prepare($query);
    if ( false===$query ) {
        die('prepare() failed: ' . htmlspecialchars($mysqli->error));
    } 
    if ( false===$query->bind_param("ssss", $senha, $primeiroNome, $ultimoNome, $email) ) {
        // again execute() is useless if you can't bind the parameters. Bail out somehow.
        die('bind_param() failed: ' . htmlspecialchars($query->error));
    }
    if ( false===$query->execute() ) {
        die('execute() failed: ' . htmlspecialchars($query->error));
    }

    $userId = $conexao->insert_id;

    $query = "SELECT * FROM profissional WHERE id_usuario = (?)";
    $query = $conexao->prepare($query);
    $query->bind_param("i", $userId);
    $query->execute();
    $query = $query -> get_result();
    if ($query->num_rows > 0){ 
    }else{
        $nomeUsuario = utf8_decode($_POST['primeiroNome']);
        $query = "INSERT INTO `profissional` (`id`, `id_usuario`, `nome_artistico`, `descricao`)
        VALUES (NULL, (?), (?), '')";
        $query = $conexao->prepare($query);
        if ( false===$query ) {
            die('prepare() failed: ' . htmlspecialchars($mysqli->error));
        } 
        if ( false===$query->bind_param("is", $userId, $nomeUsuario) ) {
            // again execute() is useless if you can't bind the parameters. Bail out somehow.
            die('bind_param() failed: ' . htmlspecialchars($query->error));
        }
        if ( false===$query->execute() ) {
            die('execute() failed: ' . htmlspecialchars($query->error));
        }
    }

    $key = md5(2418*2+$email);
    $addKey = substr(md5(uniqid(rand(),1)),3,10);
    $key = $key . $addKey;

    $query = "INSERT INTO novo_usuario (id_usuario, email, `key`) VALUES (?, ?, ?)";
    $query = $conexao->prepare($query);
    if ( false===$query ) {
        http_response_code(400);
        die('prepare() failed: ' . htmlspecialchars($mysqli->error));
    } 
    if ( false===$query->bind_param("iss", $userId, $email, $key) ) {
        http_response_code(400);
        die('bind_param() failed: ' . htmlspecialchars($query->error));
    }
    if ( false===$query->execute() ) {
        http_response_code(400);
        die('execute() failed: ' . htmlspecialchars($query->error));
    }
    $conexao->close();


    session_start();
    $_SESSION['loggedin'] = true;
    $_SESSION['usuario'] = utf8_decode($_POST['email']);
    $_SESSION['idUsuario'] = $userId;
   
    ob_start();
    echo "OK";
    $size = ob_get_length();
    header("Content-Encoding: none");
    header("Content-Length: {$size}");
    header("Connection: close");
    ob_end_flush();
    ob_flush();
    flush();

    $output='<p>Caro usuário,</p>';
    $output.='<p>Obrigado por se cadastrar no Musitray! Para confirmar o seu e-mail basta clicar no link abaixo.</p>';
    $output.='<p></p>';
    $output.='<p><a href="https://musitray.com/servicos/activateUser.php?key='.$key.'&email='.$email.'" target="_blank">
    https://musitray.com/servicos/activateUser.php?key='.$key.'&email='.$email.'</a></p>';		
    $output.='<p></p>';
    $output.='<p>Caso você não tenha realizado o cadastro na nossa plataforma, favor desconsiderar esse e-mail.</p>';   	
    $output.='<p>Obrigado por estar conosco!</p>';
    $output.='<p>Equipe Musitray</p>';
    $subject = "Ativação de conta - Musitray";


    $sender = 'noreply@musitray.com';
    $senderName = 'Musitray';

    $recipient = $email;

    $usernameSmtp = 'AKIA3KEXBKJUCJC5V2ZM';
    $passwordSmtp = 'BJX3VuKsyJ5In9gGELYixpnKWjXImgII/wO8wdb6IvX8';

    $host = 'email-smtp.us-east-1.amazonaws.com';
    $port = 587;

    $bodyText =  $output;
    $bodyHtml = $output;

    $mail = new PHPMailer(true);

    try {
        $mail->CharSet = 'UTF-8';
        $mail->Encoding = 'base64';
        $mail->isSMTP();
        $mail->setFrom($sender, $senderName);
        $mail->SMTPDebug  = 2;
        $mail->Username   = $usernameSmtp;
        $mail->Password   = $passwordSmtp;
        $mail->Host       = $host;
        $mail->Port       = $port;
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = 'tls';
        $mail->addCustomHeader('X-SES-CONFIGURATION-SET', $configurationSet);

        // Specify the message recipients.
        $mail->addAddress($recipient);
        // You can also add CC, BCC, and additional To recipients here.

        // Specify the content of the message.
        $mail->isHTML(true);
        $mail->Subject    = $subject;
        $mail->Body       = $bodyHtml;
        $mail->AltBody    = $bodyText;
        if(!$mail->Send()){
            http_response_code(400);
            die("Ocorreu um erro ao enviar o e-mail");
        }
    } catch (phpmailerException $e) {
        http_response_code(400);
        echo "An error occurred. {$e->errorMessage()}", PHP_EOL; //Catch errors from PHPMailer.
    } catch (Exception $e) {
        http_response_code(400);
        echo "Email not sent. {$mail->ErrorInfo}", PHP_EOL; //Catch errors from Amazon SES.
    }
} catch (Exception $e) {
    die('Exceção capturada: '.  $e->getMessage());
}

?>
