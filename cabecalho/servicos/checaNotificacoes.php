<?php
include "../../sessao.php";

$query = "SELECT COUNT(*) as quantidade FROM projeto_envio
            WHERE (id_profissional in (SELECT id FROM profissional WHERE id_usuario = (?)) AND fl_notificacao_profissional = 1)
            OR id_projeto in (SELECT id FROM projeto WHERE id_usuario = (?) AND fl_notificacao_dono = 1)";

if (!$query = $conexao->prepare($query)){
        die('prepare() failed: ' . htmlspecialchars($conexao->error));  
}

if (!$query->bind_param("ii", $_SESSION['idUsuario'], $_SESSION['idUsuario'])){
        die('bind_param() failed: ' . htmlspecialchars($query->error));
}

if(!$query->execute()){
        die('execute() failed: ' . htmlspecialchars($conexao->error));
        $conexao->close();
}
$res = $query->get_result();

$notificacoes = array();
$n = $res -> fetch_assoc();
$notificacoes['quantidade'] = $n['quantidade'];


$conexao->close();
echo json_encode($notificacoes)
?>