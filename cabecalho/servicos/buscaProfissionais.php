<?php
include "../../servicos/conectasql.php";

$_GET['term'] = "%".$_GET['term']."%";
$_GET['term'] = trim(strip_tags(utf8_decode($_GET['term'])));

$query = "SELECT * FROM `profissional` WHERE lower(nome_artistico) LIKE
lower((?)) ORDER BY nome_artistico LIMIT 20";
$query = $conexao->prepare($query);

$query->bind_param("s", $_GET['term']);

$query->execute();
$res = $query->get_result();
$nomes = array();

$i = 0;
while ($n = $res -> fetch_assoc()) {
    $nomes[$i]["id"] = utf8_encode($n['id']);
    $nomes[$i]["label"] = utf8_encode($n['nome_artistico']);
    $nomes[$i]["value"] = utf8_encode($n['nome_artistico']);
    $i = $i + 1;
}
$conexao->close();
echo json_encode($nomes);
?>