<meta charset="utf-8">
<script src="../js/sweetalert.min.js"></script>
<nav class="navbar navbar-expand-md navbar-light fixed-top pt-4 cabecalho bg-light"> 
    <a class="navbar-brand logo-cabecalho" href="../home/home.php">
        <img class="d-block mx-auto p-1" id="img_home" src="../images/logo.png" height="40px">
    </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <form class="form-inline input-busca ml-3" style="margin: 0;">
            <div class="input-group w-100">
                <input id="inputNavProfissionais" type="text" class="form-control input-quadrado" placeholder="Encontre profissionais...">
            </div>
        </form>
    <div class="collapse navbar-collapse bg-light" id="navbarCollapse">
        <ul class="navbar-nav ml-4 nav" style="display: -webkit-inline-box;">
            <li class="nav-item">
                <a class="nav-link pointer link-cabecalho mr-3 ml-3" href="../novoProjeto/novoProjeto.php"><b>Publique um projeto</b></a>
            </li>
            <li class="nav-item">
                <a class="nav-link pointer link-cabecalho mr-3 ml-3" href="../meuPerfil/meuPerfil.php"><b>Meu perfil e projetos </b>
                    <span id="notification-badge" class="badge badge-pill badge-danger" style="visibility: hidden;"></span>
                </a>
            </li>
        </ul>
    </div>
    <span>
        <a class="nav-link pointer link-cabecalho float-right" href="../servicos/logout.php"><b>Sair</b></a>
    </span>
</nav>
<div class="row" style="height: 15vh"></div>

<div class="loading-modal">
    <svg class="loading-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgba(255, 255, 255, 0); display: block;" width="201px" height="201px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
    <g transform="rotate(180 50 50)"><rect x="2.761904761904762" y="31.25" width="4" height="10.3091" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.7142857142857142s"></animate>
        </rect><rect x="7.523809523809524" y="31.25" width="4" height="37.2134" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.11904761904761904s"></animate>
        </rect><rect x="12.285714285714286" y="31.25" width="4" height="6.63531" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.6547619047619048s"></animate>
        </rect><rect x="17.047619047619047" y="31.25" width="4" height="22.885" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.3571428571428571s"></animate>
        </rect><rect x="21.80952380952381" y="31.25" width="4" height="5.25778" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.5357142857142857s"></animate>
        </rect><rect x="26.571428571428573" y="31.25" width="4" height="24.0574" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.8928571428571428s"></animate>
        </rect><rect x="31.333333333333336" y="31.25" width="4" height="13.651" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.4166666666666666s"></animate>
        </rect><rect x="36.095238095238095" y="31.25" width="4" height="37.473" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.17857142857142855s"></animate>
        </rect><rect x="40.857142857142854" y="31.25" width="4" height="7.66924" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.47619047619047616s"></animate>
        </rect><rect x="45.61904761904762" y="31.25" width="4" height="35.8799" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.05952380952380952s"></animate>
        </rect><rect x="50.38095238095238" y="31.25" width="4" height="29.4146" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-1.1309523809523807s"></animate>
        </rect><rect x="55.142857142857146" y="31.25" width="4" height="26.6141" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-1.0714285714285714s"></animate>
        </rect><rect x="59.904761904761905" y="31.25" width="4" height="25.2844" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-1.0119047619047619s"></animate>
        </rect><rect x="64.66666666666667" y="31.25" width="4" height="24.9828" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.9523809523809523s"></animate>
        </rect><rect x="69.42857142857143" y="31.25" width="4" height="15.9884" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.7738095238095237s"></animate>
        </rect><rect x="74.19047619047619" y="31.25" width="4" height="33.075" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="0s"></animate>
        </rect><rect x="78.95238095238095" y="31.25" width="4" height="5.1567" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.5952380952380952s"></animate>
        </rect><rect x="83.71428571428571" y="31.25" width="4" height="31.3164" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.2976190476190476s"></animate>
        </rect><rect x="88.47619047619048" y="31.25" width="4" height="21.1825" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.8333333333333331s"></animate>
        </rect><rect x="93.23809523809524" y="31.25" width="4" height="35.9767" fill="#18adbe">
        <animate attributeName="height" calcMode="spline" values="25;37.5;5;25" times="0;0.33;0.66;1" dur="1.1904761904761905s" keySplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" repeatCount="indefinite" begin="-0.23809523809523808s"></animate>
        </rect>
    </g>
    </svg>
</div>

<script src="../cabecalho/cabecalho.js" crossorigin="anonymous"></script>