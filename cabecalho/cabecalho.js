$(document).ready(function () {
    var url = window.location;
    $('ul.nav a[href="' + url + '"]').parent().addClass('active');
    $('ul.nav a').filter(function () {
        return this.href == url;
    }).parent().addClass('ativo');

    $("#inputNavProfissionais").autocomplete({
        source: "../cabecalho/servicos/buscaProfissionais.php",
        minLength: 0,
        select: function (event, ui) {
            if (ui.item) {
                window.open("../perfilProfissional/perfilProfissional.php?id=" + ui.item.id);
            } else {
            }
        }
    });

    $.ajax({
        url: "../cabecalho/servicos/checaNotificacoes.php",
        method: "GET",
        success: function (resposta) {
            try {
                resposta = JSON.parse(resposta);
                if (parseInt(resposta.quantidade) > 0) {
                    $("#notification-badge").css('visibility', 'visible');
                    $("#notification-badge").html(resposta.quantidade);
                } else {
                    $("#notification-badge").css('visibility', 'hidden');
                }
            } catch {
                swal("Erro", "Um erro inesperado ocorreu ao carregar as notificações", "error");
            }
        }
    });
});

ajaxRequestsCount = 0;
$(document).on({
    ajaxStart: function () {
        ajaxRequestsCount++;
        setTimeout(_ => {
            if (ajaxRequestsCount > 0) {
                $("body").addClass("loading");
            }
        }, 400);
    },
    ajaxStop: function () {
        ajaxRequestsCount--;

        if (ajaxRequestsCount == 0)
            $("body").removeClass("loading");
    }
});  