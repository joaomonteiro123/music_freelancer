<?php
include "../sessao.php";
$_SESSION['idPerfil'] = $_GET['id'];
?>

<HTML>
<HEAD>
<meta charset="utf-8">
    <TITLE>Musitray - Sua música a um clique</TITLE>
    <link rel="stylesheet" href="../bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/estilo.css">
    <script src="../js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/combobox.js"></script>
    <script src="../js/jquery.filterizr-with-jquery.min.js"></script>
    <script src="../js/jquery.filterizr.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="../js/vue.js"></script>
    <script src="../js/vue-resource.js"></script>
    <script src="../js/vue-router.js"></script>
</HEAD>

<script  type="text/x-template" id="portfolio-template">
    <div class="col-4 d-flex mb-3">
        <div class="card w-100">
            <div v-if="projeto.tipo == 'youtube'" class="embed-responsive embed-responsive-16by9">
                <iframe class="resp-iframe" :src="linkYouTube(projeto.link)"
                    frameborder="0" allowfullscreen>
                </iframe>  
            </div>
            <div v-if="projeto.tipo == 'youtube'" class="card-body portfolio-wrapper">
                <div class="nome-portifolio">{{projeto.nome}}</div>
                <div class="decricao-campo">{{projeto.descricao}}</div>
            </div>

            <div v-else-if="projeto.tipo == 'spotify'" v-html="linkSpotify(projeto.link)">
            </div>
        </div>
    </div>
</script>

<BODY>   
<?php 
    include "../cabecalho/cabecalho.php"
?>
<div class="container" id="app">
    <div class="menu-lateral-perfil bg-light">
        <div class="row">
            <div class="col-12">
                <div class="foto-perfil">
                    <img :src="profissional.foto">
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-12">
                <label class="texto-preto medio"><b>{{profissional.nomeProfissional}}</b></label>
            </div>
        </div>
        <div class="row">
            <div class="col-12 pl-4 pr-4">
                <p>{{profissional.descricao}}</p>
            </div>
        </div>
    </div>

    <div class="row pt-4">
        <div class="col-9 offset-3">
            <div class="row">
                <div class="col-12">
                    <label class="texto-principal grande">
                        <b>{{profissional.nomeProfissional}}</b>
                    </label>
                    <div class="row">
                        <div class="col-12">
                            <p>{{profissional.descricao}}</p>
                        </div>
                    </div>
                </div>
                <div class="separador"></div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-12">
                            <label class="texto-principal medio">
                                <b>Instrumentos e Especialidades</b>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 ml-1">
                                    <label class="texto-principal small">Instrumentos</label>
                                </div>
                            </div>
                            <div class="row p-1">
                                <div class="col-12">
                                    <textarea type="text" class="form-control input disabled textarea w-100 bg-light" :value="instrumentos.map(e => e.nome).join(', ')" disabled></textarea>                                                                                                                                                 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 ml-1">
                                    <label class="texto-principal small">Especialidades</label>
                                </div>
                            </div>
                            <div class="row p-1">
                                <div class="col-12">
                                    <textarea type="text" class="form-control input disabled textarea w-100" :value="especialidades.map(e => e.descricao).join(', ')" disabled></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-12">
                            <label class="texto-principal medio">
                                <b>Cursos e Qualificações</b>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div v-for="(curso, index) in cursos" class="row p-1">
                                <div class="col-12">
                                    <textarea type="text" class="form-control input disabled textarea w-100 bg-light" :value="curso.descricao" disabled></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="separador"></div>            
            <div v-if="portfolio.length">
                <div class="row pt-2">
                    <div class="col-12">
                        <label class="texto-principal medio">
                            <b>Portfólio</b>
                        </label>
                    </div>
                </div>
                <div class="row listaProjetosPortifolio">
                    <portfolio
                        v-for="projeto in portfolio"
                        v-bind:key="projeto.id"
                        :projeto="projeto"
                    ></portfolio>
                </div>
            </div>
        </div>
    </div>
</div>   

<?php 
    include "../rodape/rodape.php"
?>

<script src="../js/popper.min.js" crossorigin="anonymous"></script>
<script src="perfilProfissional.js"></script>
<script src="../bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script> 
</BODY>
</HTML>