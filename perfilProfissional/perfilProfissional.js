Vue.component("portfolio", {
    props: ["projeto"],
    template: '#portfolio-template',
    methods: {
        linkYouTube: function (link) {
            n = link.lastIndexOf('/')
            return "https://www.youtube.com/embed/" + link.substring(n + 1)
        },
        linkSpotify(link) {
            let newLink = link.slice(0, 25) + 'embed/' + link.slice(25);
            width = ($('.listaProjetosPortifolio').width() / 3) - 32;
            return `<iframe src='${newLink}' class='card-projetoSpotify'
                        width="${width}" height="${width * 1.26}" frameborder="0" allowtransparency="true" allow="encrypted-media">
                    </iframe>`
        }
    },
})

meuPerfil = new Vue({
    el: "#app",
    data: {
        profissional: [],
        instrumentos: [],
        portfolio: [],
        especialidades: [],
        cursos: [],
    },
    created: async function () {
        $("body").addClass("loading");
        await this.$http.get("servicos/carregaProfissionais.php").then(function (resposta) {
            this.profissional = resposta.data;
            this.instrumentos = this.profissional.instrumentos;
            this.portfolio = this.profissional.portfolio;
            this.cursos = this.profissional.cursos;
            this.especialidades = this.profissional.especialidades;
            Vue.nextTick().then(function () {
                $(".textarea").each((index, element) => {
                    element.style.height = "";
                    element.style.height = element.scrollHeight + "px";
                });
                $("body").removeClass("loading");
            })
        });
    },
    methods: {
    },
})