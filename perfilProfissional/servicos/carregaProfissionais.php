<?php
include "../../sessao.php";

$idProfissional = $_SESSION['idPerfil'];
$_SESSION['idPerfil'] = null;

$query = "SELECT p.nome_artistico as nome, p.id as id, u.foto, p.descricao, u.id as idUsuario
            FROM profissional p JOIN usuario u ON u.id = p.id_usuario
            WHERE p.id = (?) GROUP BY p.id";

$query = $conexao->prepare($query);
$query ->bind_param('i',$idProfissional);
$query->execute();

$res = $query->get_result();

$profissional = array();
$n = $res -> fetch_assoc();

$profissional['nomeProfissional'] = utf8_encode($n['nome']);
$profissional['descricao'] = utf8_encode($n['descricao']);
$profissional['idProfissional'] = $n['id'];
$idUsuario = $n['idUsuario'];
if ( $n['foto'] == "" ){
    $n['foto'] = "../images/generic_user.png";
}else{
    $n['foto'] = "../servicos/imagemUsuario.php?id=".$idUsuario;
}   
$profissional['foto'] = $n['foto'];


$query = "SELECT DISTINCT i.id, i.nome FROM profissional_instrumentos pi
    INNER JOIN instrumentos i ON i.id = pi.id_instrumentos
    WHERE pi.id_profissional = (?)";
$query = $conexao->prepare($query);
$query -> bind_param('i',$idProfissional);
if ($query->execute() === TRUE) {
} else {
    echo "erro";
}
$res = $query->get_result();
$instrumentos = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $instrumentos[$i]['nome'] = utf8_encode($n['nome']);
    $instrumentos[$i]['id'] = $n['id']; 
    $i = $i + 1;
}
$profissional['instrumentos'] = $instrumentos;


$query = "SELECT DISTINCT pp.id, pp.link, pp.tipo, pp.nome, pp.descricao FROM portfolioprofissional pp
INNER JOIN profissional p ON p.id = pp.id_profissional
WHERE p.id = (?)";
$query = $conexao->prepare($query);
$query -> bind_param('i',$idProfissional);
if ($query->execute() === TRUE) {
} else {
    echo "erro";
}
$res = $query->get_result();
$portfolio = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $portfolio[$i]['link'] = utf8_encode($n['link']);
    $portfolio[$i]['tipo'] = utf8_encode($n['tipo']);
    $portfolio[$i]['nome'] = utf8_encode($n['nome']);
    $portfolio[$i]['descricao'] = utf8_encode($n['descricao']);
    $portfolio[$i]['id'] = $n['id']; 
    $i = $i + 1;
}
$profissional['portfolio'] = $portfolio;


$query = "SELECT DISTINCT e.id, e.descricao FROM profissional_especialidades pe INNER JOIN especialidades e
ON e.id = pe.id_especialidades INNER JOIN profissional p ON p.id = pe.id_profissional
WHERE p.id = (?)";
$query = $conexao->prepare($query);
$query -> bind_param('i',$idProfissional);
if ($query->execute() === TRUE) {
} else {
    echo "erro";
}
$res = $query->get_result();
$especialidades = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $especialidades[$i]['descricao'] = utf8_encode($n['descricao']);
    $especialidades[$i]['id'] = $n['id']; 
    $i = $i + 1;
}
$profissional['especialidades'] = $especialidades;
    
$query = "SELECT DISTINCT cp.id, cp.descricao FROM curso_profissional cp
INNER JOIN profissional p ON p.id = cp.id_profissional
WHERE p.id = (?)";
if (!$query = $conexao->prepare($query)){
    die('prepare() failed: ' . htmlspecialchars($conexao->error));
}
if (!$query -> bind_param('i',$idProfissional)){
    die('bind_param() failed: ' . htmlspecialchars($query->error));
}
if(!$query->execute()){
    die('execute() failed: ' . htmlspecialchars($conexao->error));
}
$res = $query->get_result();
$cursos = array();
$i = 0;
while ($n = $res -> fetch_assoc()) {
    $cursos[$i]['descricao'] = utf8_encode($n['descricao']);
    $cursos[$i]['id'] = $n['id']; 
    $i = $i + 1;
}
$profissional['cursos'] = $cursos;


$conexao->close();
echo json_encode($profissional);
?>